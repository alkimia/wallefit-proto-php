<?php
/* MVC Simple*/
define('simplemvc_exec', 1);
chdir('..');

if( !file_exists('base/config.php') )
{
    die('Falta archivo de configuracion');

}
else
{
   /// cargamos las constantes de configuración
   require_once('base/Controller.php');
   require_once('base/config.php');
   require_once('base/bd.php');
   require_once('base/Users.php');
   
    $banned = array(
        "INNER",
        "!--",
        "SCRIPT",
        "SELECT",
        "JOIN",
        "'");
        

    foreach($_POST as $key => $value) {
        $value = str_replace($banned, "", $value);
        
        $urldata[$key] = strip_tags ($value); 
    }
    
    foreach($_GET as $key => $value) {
        $value = str_replace($banned, "", $value);
        
        $urldata[$key] = strip_tags ($value); 
    }
    
    require("controller/ajax_controller.php");

    $app = new ajax_controller('',$urldata);
    
    $app->Run();    
}    
?>    