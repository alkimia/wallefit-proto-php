<?php
require_once 'Users.php';

class Controller {
    public $user_logged = false;
    
    public function __construct($title = '', $urldata ='') {
         $this->page =
                 array(
                     'title' => $title,
                 );
                 
         $this->recoverySession();
         $this->urldata = $urldata;
         
         $this->success_message = '';
         $this->info_message = '';
         $this->warning_message = '';
         $this->danger_message = '';       
         
        $this->checkUserDataCompleted();
    }
    
    public function checkUserDataCompleted() {
        $db = new Database();
        
        if ($_GET['premsg'] == '' && $this->user_logged) {
        
            $sql = "SELECT * FROM users WHERE id='$this->id'";
            
            $db->atline = 29;        
            $row = $db->getRow($sql);        
    
            if ($row['email'] == '' 
            || $row['name'] == '' 
            || $row['surname'] == '' 
            || $row['birthdate_yy'] == ''
            || $row['birthdate_mm'] == ''
            || $row['birthdate_dd'] == ''
            || $row['postalcode'] == '') {
                
                header('Location: index.php?page=perfil&premsg=fill_fields');            
            }
        
        }
                  
    }
    
    public function recoverySession() {
        if (isset($_COOKIE['simplemvc_user'])) {
            $data_cookie = json_decode($_COOKIE['simplemvc_user']);
            if ($data_cookie->id != '') {
                $this->id = $data_cookie->id;
                $this->user_data = $data_cookie;
                $this->user_logged = true;
            } else {
                $this->user_logged = false;    
            }
        } else {
            $this->user_logged = false;
            
        }
    }
    
    public function whoami() {
        require('view/layouts/userbox.php');
    }     
        
    public function accessControlPage($params) {
        extract($params);
        
        if ($this->user_logged) {
            // check rol            
        } else {
            $url = 'index.php?page=' . $redir_not_login;
            header('Location: '.$url);
            

        }                
    }
    
    public function doLogin($params) {        
        $user = new User();
        
        if ($user->login($params)) {
            $this->user_data = $user->loadUser();
            extract($params);
            $url = 'index.php?page=' . $redir_is_loggin;
            header('Location: '.$url);
                  
        } else {
            return false;
        }     
    }
    
    public function doLogout($params) {        
        $user = new User();
        
        $user->destroySession();
        $url = 'index.php?page=login';
        header('Location: '.$url);      
    }    
    
    public function renderPage($tpl, $view_data) {
        $this->title = $view_data['title'];
        require("view/" . $tpl .".php");
    }
    
    public function addScripts($jscript) {
        echo '<script>';
        include('view/assets/js/' . $jscript);
        echo '</script>';
    }
    
    public function addStyles($jscript) {
        include('view/assets/css/' . $jscript);
    }    
    
    public function Header() {
        require('view/layouts/header.php');        
    }
    
   public function accountFilter($params) {
    extract($params);
    
    // traspasa el id de usuario a la clase wallet
    $wallet = new wallet($this->id);    
    
    if ($this->urldata['id_account'] == '') {
        $id_account = $wallet->getMainAccount(); 
    } else {
        $id_account = $this->urldata['id_account'];
    }

    $accounts = $wallet->getUserAccounts();
        
    require('view/layouts/account_filter.php');
   }
   
   public function messagesBox($params) {
    require('view/layouts/messages.php');    
   }  
   
   public function card_categories($params) {
    require('view/layouts/card_categories.php');
   }
   
   public function card_timelime_detail($params) {
    require('view/layouts/card_timeline_detail.php');
   }
   
   public function card_timelime_simple($params) {
    require('view/layouts/card_timeline_simple.php');
   }
   
   public function card_users_sharing($params) {
    require('view/layouts/card_sharing.php');
   }
   
   public function card_users_search($params) {
    require('view/layouts/card_share_search.php');
   }
   
   public function perfilTabs($params) {
    require('view/layouts/perfil_tabs.php');    
   }
   
   public function latest($params) {
    require('view/layouts/latest.php');
   }
   
}