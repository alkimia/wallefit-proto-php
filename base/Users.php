<?php
class User {
    public function getProfile() {
        $db = new Database();
        
        extract($data);  
        
        $sql = "SELECT * FROM users WHERE id=$this->id_user";
        $db->atline = 9;        

        return $db->getRow($sql);           
    }
    public function updateProfile($data) {
        $db = new Database();
        
        extract($data);  
        
        $sql = "UPDATE users 
        SET description='$description',
        postalcode='$cp',
        email='$email',
        name='$name',
        surname='$surname',
        birthdate_yy='$yy',
        birthdate_mm='$mm',
        birthdate_dd='$dd',
        secret_question='$secret_question',
        secret_answer='$secret_answer',
        version=2         
        WHERE id=$this->id_user";
        $db->atline = 28;        
        $db->updateRow($sql);
        
        if ($pass != '') {
            $cryptedpass = md5($pass);            
            
            $sql = "UPDATE users 
            SET password='$cryptedpass'
            WHERE id=$this->id_user";
            $db->atline = 37;        
            $db->updateRow($sql);            
        }
 
         
        $response['success'] = true;
        $response['success_message'] = 'Datos de perfil actualizados';
        
        return $response;                
    }
    
    public function ifUserExists($username) {
        $db = new Database();           
        
        // evitar duplicidades
        $sql = "SELECT username FROM users
        WHERE username='$username'";
        //echo $sql;

        $db->atline = 58;        
        $row = $db->getRow($sql);
        
        if ($row['username'] == $username) {
            return true;
        } else {
            return false;
        }           
    }
    
    public function checkSecrets($params) {
        $db = new Database();        
        extract($params);      
        
        $sql = "SELECT * FROM users 
            WHERE username='$username'
            AND secret_answer='$secret_answer'";
                                    
        $row = $db->getRow($sql);
        
        if ($row['secret_answer'] == $secret_answer) {
            return true;
        } else {
            return false;
        }              
    }
    
    public function checkRePasswd($params) {
        $db = new Database();        
        extract($params);

        if ($pass == $repass) {
            $sql = "SELECT * FROM users 
            WHERE username='$username'
            AND secret_answer='$secret_answer'";                

            $row = $db->getRow($sql);
            if ($row['secret_answer'] == $secret_answer) {
                return true;
            } else {
                return false;
            }            
        }
    }
    
    public function reSetPasswd($params) {
        $db = new Database();        
        extract($params);
        
        $crypted_password = md5($pass);

        $sql = "UPDATE users 
        SET password='$crypted_password'
        WHERE username='$username'
        AND secret_answer='$secret_answer'";                                

        $db->atline = 23;  
        $db->updateRow($sql);
    }
    
    public function getSecrets($params) {
        $db = new Database();
        
        extract($params);
        
        $sql = "SELECT * FROM users WHERE username='$username'";                
        $row = $db->getRow($sql);
        
        return $row['secret_question'];                        
    }
    
    public function createUser($data) {
        $db = new Database();
        
        extract($data);   
        
        // evitar duplicidades
        $sql = "SELECT username FROM users
        WHERE username='$name'";

        $db->atline = 23;        
        $row = $db->getRow($sql);           
        
        if ($row['username'] == $name) {
            $response['success'] = false;
            $response['message_error'] = 'Nombre de usuario ya registrado';
            
        } else {
                     
            $sql = "INSERT INTO users
                (username, password, register_date)
                VALUES
                (:username, :password, :register_date)";
                
            $crypted_password = md5( $password );            
    
            $params = array (
                ":username" => $name,
                ":password" => $crypted_password,
                ":register_date" => date("Y-m-d H:i:s")
            );            

            $db->atline = 33;                   
            $db->insertRow($sql, $params);
            
            $id_owner = $db->last_id;
            
            // crear cuenta principal
            $sql = "INSERT INTO cuentas
            (id_owner,
            descripcion,
            principal,
            system)
            VALUES
            ('$id_owner',
            'Personal principal',
            1,
            1)";
            
            $db->atline = 48;               
            $db->insertRow($sql);
            
            $id_account = $db->last_id;
            
            // crear notificaciones bsaicas
            $sql="INSERT INTO users_notifications
            (id_owner, id_notification)
            VALUES
            ($id_owner,1)";
            $db->atline = 94;               
            $db->insertRow($sql);             
            
            // crear permisos sobre la cuenta
            $sql = "INSERT INTO cuentas_permisos
            (id_account,
            id_owner,
            to_user,
            can_r,
            can_w)
            VALUES
            ($id_account,
            $id_owner,
            $id_owner,
            1,
            1)";
            $db->atline = 94;               
            $db->insertRow($sql);                                                            
            
            // TO-DO: Mejorar esto. Pasarlo a la clase wallet
            // crear categorias de gasto e ingreso
            
            $categorias_gasto = array (
                'Comida' => 'O', 
                'Coche' => 'O',
                'Ocio' => 'O',
                'Farmacia' => 'O', 
                'Telef�nia' => 'O', 
                'Casa' => 'O',
                'Ropa' => 'O',
                'Transporte' => 'O',
                'Electricidad - Gas' => 'O',
                'Ahorros' => 'I',
                'Financieros' => 'I',
                'Sueldo' => 'I',
                'Pensiones' => 'I'
            );
            
            foreach($categorias_gasto as $key => $value) {
                
                
                $keyname = utf8_encode ($key);
                
                $sql = "INSERT INTO grupos_apuntes
                (id_owner, 
                id_cuenta,
                descripcion,
                tipo,
                system)
                VALUES
                ($id_owner,
                $id_account,
                '$keyname',
                '$value',
                1)";
                
                $db->atline = 81;
                $db->insertRow($sql);                                   
                
            }
            
            $login_data = array (
                "name" => $name,
                "password" => $password
            );
            
            $params = array (
                'login_data' => $login_data
            );  
            
            $this->Login($params);            
            
            $response['success'] = true;
            $response['success_message'] = $data;             
        }
        
        return $response;                                                
    } 
        
    public function Login($params) {
        $db = new Database();
                
        extract ($params);
        
        $sql = "SELECT * FROM users WHERE username='" . $login_data['name'] . "' AND password='" . md5($login_data['password']) . "'";        
        
        $row = $db->getRow($sql);
        
        if ($row && $row['username'] == $login_data['name']) {
            $this->createSession($row);
            return true;
                        
        } else {
            return false;
        }
    }
    
    public function loadUser() {
        if ($this->id != '') {
            return false;
        }
        
        $db = new Database();                
        
        $sql = "SELECT * FROM users WHERE id='" . $this->id . "'";        
        
        $this->user_data = $db->getRow($sql, $params_query);         
        
    }
    
    public function createSession($row) {
        $cookie_content = json_encode($row);        
        $this->id = $row['id'];
        setcookie("simplemvc_user", $cookie_content, time() + (10 * 365 * 24 * 60 * 60));        
    }
    
    public function destroySession() {
        $this->id = '';
        setcookie("simplemvc_user", '', time()-3600);        

    }
}