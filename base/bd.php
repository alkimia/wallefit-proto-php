<?php 
class Database{    
    // connect to db
    public function __construct(){
        $this->isConn = TRUE;
        try {
            $config = new Config();
            $host = $config->host;
            $username = $config->username;
            $password = $config->password;
            $dbname = $config->dbname;
            $options = $config->options;
            
            $this->datab = new PDO("mysql:host={$host};dbname={$dbname};charset=utf8", $username, $password, $options);
            $this->datab->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->datab->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            die('Error de conexion a la base de datos');        
        }
        
    }
    
    // disconnect from db
    public function Disconnect(){
        $this->datab = NULL;
        $this->isConn = FALSE;
    }
    // get row
    public function getRow($query,$params=[]){
        try {
            $stmt=$this->datab->prepare($query);
            $stmt->execute($params);
            return $stmt->fetch();        
        } catch (PDOException $e) {
            //throw new Exception($e->getMessage());
            echo $query;
            echo '<br>En linea: ' . $this->atline . '<br>';
            die('Error fatal');
        }    
    }
    // get rows
    public function getRows($query,$params=[]){
        try {
            $stmt=$this->datab->prepare($query);
            $stmt->execute($params);
            return $stmt->fetchAll();        
        } catch (PDOException $e) {
            //throw new Exception($e->getMessage());
            echo $query;
            echo '<br>En linea: ' . $this->atline . '<br>';
            die('Error fatal');
        }          
    }
    
    // get rows
    public function getMatchs($query, $like){
        try {
            $stmt=$this->datab->prepare($query);
            $stmt->bindValue(1, "%$like%", PDO::PARAM_STR);
            $stmt->execute();            
            
            return $stmt->fetchAll();        
        } catch (PDOException $e) {
            throw new Exception($e->getMessage());
        }          
    }    
    // insert row
    public function insertRow($query,$params=[]){
        try {
            $smtm = $this->datab->prepare($query);                                
            foreach($params as $key => $value) {
                $smtm->bindValue($key, $value);
            }
            
            $smtm->execute(); 
            
            $this->last_id = $this->datab->lastInsertId();  
        } catch (PDOException $e) {
            //throw new Exception($e->getMessage());
            echo $query;
            echo '<br>En linea: ' . $this->atline . '<br>';
            die('Error fatal');
        } 
        
 
    }
    // update row
    public function updateRow($query,$params=[]){
        try {
            $smtm = $this->datab->prepare($query);                                
            foreach($params as $key => $value) {
                $smtm->bindValue($key, $value);
            }
            
            $smtm->execute();
        } catch (PDOException $e) {
            echo $e->getMessage();
            echo $query;
            echo '<br>En linea: ' . $this->atline . '<br>';
            die('Error fatal');
        }         
    }
    // delete row
    public function deleteRow($query,$params=[]){
        try {
            $smtm = $this->datab->prepare($query);                                
            foreach($params as $key => $value) {
                $smtm->bindValue($key, $value);
            }
            
            $smtm->execute();
        } catch (PDOException $e) {
            //throw new Exception($e->getMessage());
            echo $query;
            echo '<br>En linea: ' . $this->atline . '<br>';
            die('Error fatal');
        }   
        
    }
}

?>