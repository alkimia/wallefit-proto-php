<?php
require("model/wallet.php");

class dashboard extends Controller {
    
   public function Run() {
    
    $params = array (
        "redir_not_login" => 'login',
        "rol_required" => false
    );

    $this->accessControlPage($params);
    
    $wallet = new wallet($this->id);

    if ($this->urldata['id_account'] == '') {
        $id_account = $wallet->getMainAccount(); 
    } else {
        $id_account = $this->urldata['id_account'];
    }
    
    // tiene permisos sobre la cuenta?
    $params_account_permission = array (
        "id_account" => $id_account,
        "id_user" => $this->id,
        "can_write" => 1
    );
    
    if (!$wallet->checkUserAccountPermissions($params_account_permission)) {
        header('Location: index.php?page=error');
    }
    
    
    
    
    if ($this->urldata['action'] == 'delete_cat') {
        $params_manage_categories = array (
            "id" => $this->urldata['cat_id'],
            "id_account" => $id_account,
            "action" => "delete_category"
        );
        $cat_manager = $wallet->manageCategories($params_manage_categories);
        
        if ($cat_manager['success']) {
            $this->success_message = $cat_manager['success_message'];
        } else {
            $this->danger_message = $cat_manager['error_message'];
        }          
        
                
    } elseif ($this->urldata['action'] == 'rename_cat') {
        $params_manage_categories = array (
            "id" => $this->urldata['cat_id'],
            "id_account" => $id_account,
            "action" => "rename_category",
            "descripcion" => $this->urldata['name_cat'],
        );
        $cat_manager = $wallet->manageCategories($params_manage_categories);    
        
        if ($cat_manager['success']) {
            $this->success_message = $cat_manager['success_message'];
        } else {
            $this->danger_message = $cat_manager['error_message'];
        }             
    } elseif ($this->urldata['action'] == 'add_cat') {
        $params_manage_categories = array (
            "id_account" => $id_account,
            "action" => "add_category",
            "category_name" => $this->urldata['name_cat'],
            "type" => $this->urldata['type_sel']
        );
        $cat_manager = $wallet->manageCategories($params_manage_categories);    
        
        if ($cat_manager['success']) {
            $this->success_message = $cat_manager['success_message'];
        } else {
            $this->danger_message = $cat_manager['error_message'];
        }         
    }    
        
    if ($this->urldata['month_selected'] == '') {
        $this->urldata['month_selected'] = date("m");
    }
    
    if ($this->urldata['year_selected'] == '') {
        $this->urldata['year_selected'] = date("Y");
    }
        
    $params_get_outcome_groups = array (
        "id_account" => $id_account,
        "month" => $this->urldata['month_selected'],
        "year" => $this->urldata['year_selected'],
        "type" => 'O' 
    );    
    
    $params_get_income_groups = array (
        "id_account" => $id_account,
        "month" => $this->urldata['month_selected'],
        "year" => $this->urldata['year_selected'],
        "type" => 'I' 
    );    
          
    
    $params_balance_last_month = array (
        "id_account" => $id_account,
        "month" => $this->urldata['month_selected'],
        "year" => $this->urldata['year_selected']
    );
    
    $params_balance_this_month = array (
        "id_account" => $id_account,
        "month" => $this->urldata['month_selected'],
        "year" => $this->urldata['year_selected']
    );
    
    $params_total_debit = array (
        "field" => "debit",
        "id_account" => $id_account,
        // range => same, same month, from_to, distinct months
        "range" => "same",
        "month" => $this->urldata['month_selected'],
        "year" => $this->urldata['year_selected']        
    );
    
    $params_total_credit = array (
        "field" => "credit",
        "id_account" => $id_account,
        // range => same, same month, from_to, distinct months
        "range" => "same",
        "month" => $this->urldata['month_selected'],
        "year" => $this->urldata['year_selected']      
    );        
    
    $balance_last_month = $wallet->getBalanceLastMonth($params_balance_last_month);
    $total_debit = $wallet->getTotalMovements($params_total_debit);
    $total_credit = $wallet->getTotalMovements($params_total_credit);
    
    $balance_this_month = $balance_last_month + $total_credit - $total_debit;

    $view_data = array (
        "balance_last_month" => $balance_last_month,
        //"balance_this_month" => $wallet->getBalanceCurrentMonth($params_balance_this_month),
        "balance_this_month" => $balance_this_month,
        "outcome_groups" => $wallet->getCategoriesTotalDetail($params_get_outcome_groups),
        "income_groups" => $wallet->getCategoriesTotalDetail($params_get_income_groups),
        "month_selected" => $this->month_selected,
        "year_selected" => $this->year_selected,
        "id_account" => $id_account,
        "total_debit" => $total_debit,
        "total_credit" => $total_credit,
        "limits_account" => $wallet->checkLimitsNoPremium(),  
        "title" => "Panel de gestión"                
    );
    
    
    $this->renderPage('dashboard', $view_data);
   }
      
}