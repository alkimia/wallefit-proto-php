<?php
require("model/notifications.php");

class fireb_message extends Controller {
    
   public function Run() {
    if ($this->urldata['id_message'] == '') {
        header('Location: index.php?page=error');
    }
    
    $data_notif = new notif($this->urldata['id_message']);    
    
    if ($data_notif != 'error-data') {
        // login
        $user = new User;
        $user->createSession($data_notif->user_data);        
    }

    $view_data = array (
        "data_notif" => $data_notif->data
    );
        
    $this->renderPage('fireb_message', $view_data);
   }
      
}