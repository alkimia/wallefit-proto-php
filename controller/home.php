<?php
class home extends Controller {
    
   public function Run() {
    
    if ($_POST) {        
        if ($_POST['action'] == 'register') {
            $register_user = User::createUser($_POST);
            
            if ($register_user) {
                $url = 'Location: index.php?page=register_done';                
                header($url);
            } else {
                $url = 'Location: index.php?page=error';                
                header($url);
                
            }            
        }
    }    

    $view_data = array (
        "title" => "Wallefit"                
    );
    
    $this->renderPage('login', $view_data);
   }    
}