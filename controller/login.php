<?php
class login extends Controller {
    
   public function Run() {
    
    if ($_POST) { 
        if ($_POST['action'] == 'login') {
            $params = array (
                'redir_is_loggin' => 'dashboard',
                'login_data' => $this->urldata
            );                                    
            
            if (!$this->doLogin($params)) {
                $this->danger_message = 'Usuario no existe o clave incorrecta';    
            }  
            
        } elseif ($_POST['action'] == 'logout') {
            $this->doLogout();
        }       
    }    
    
    if ($_GET['action'] == 'logout') {
        $this->doLogout();
    }    

    $view_data = array (
        "login_status" => $this->login_status,
        "title" => "Inicie sesión"                
    );    
    
    $this->renderPage('login', $view_data);
   }    
}