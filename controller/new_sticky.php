<?php
require("model/Sticky.php");

class new_sticky extends Controller {
    
   public function Run() {
    
    $params = array (
        "redir_not_login" => 'login',
        "rol_required" => false
    );

    $this->accessControlPage($params);    
    
    if ($_POST) {        
        if ($_POST['id'] == '') {
            $id_last = $register_sticky = Sticky::createNew($_POST);
        } else {
            $register_sticky = Sticky::editSticky($_POST);
        }
            
        if ($register_sticky) {
            $url = 'Location: index.php?page=dashboard';                
            header($url);
        } else {
            $url = 'Location: index.php?page=error';                
            header($url);                
        }            
    }
    
    $sticky_data = array ();
    
    if ($_GET['id'] != '') {
        $params = array (
            "id" => $_GET['id']
        );
                
        $sticky_data = Sticky::getItem($params);
    }    
    
    $view_data = array (
        "title" => "Inserte nuevo sticky",
        "data" => $sticky_data                
    );
    
    $this->renderPage('new_sticky', $view_data);
   }    
}