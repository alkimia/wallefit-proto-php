<?php
require("model/wallet.php");

class note extends Controller {
    
   public function Run() {
    
    $params = array (
        "redir_not_login" => 'login',
        "rol_required" => false
    );

    $this->accessControlPage($params);
    
    $wallet = new wallet($this->id);
    
    if ($this->urldata['id_account'] == '') {
        $id_account = $wallet->getMainAccount(); 
    } else {
        $id_account = $this->urldata['id_account'];
    }    
    
    if ($_POST) {
        if ($_POST['action'] == 'add_category') {
            $res_manager = $wallet->manageCategories($_POST);                        
            
            if ($res_manager['success']) {
                $this->success_message = $res_manager['success_message'];
            } else {
                $this->danger_message = $res_manager['error_message'];
            }
        }
        if ($_POST['action'] == 'add_movement') {
            $res_manager = $wallet->manageMovements($_POST);                        
            
            if ($res_manager['success']) {
                $this->success_message = $res_manager['success_message'];
            } else {
                $this->danger_message = $res_manager['error_message'];
            }            
        }
    }
        

            
    
    if ($this->urldata['id_movement'] != '') {
        $params_to_manage_movements = array (
            "id_movement" => $this->urldata['id_movement'],
            "id_account" => $id_account,
            "action" => "read_movement"
        );        
        $data_movement = $wallet->manageMovements($params_to_manage_movements);        
        
        $params_get_outcome_groups = array (
            "id_cuenta" => $data_movement['success_message']['id_account'],
            "id_tipo" => 'O' 
        );        
        
        $params_get_income_groups = array (
            "id_cuenta" => $data_movement['success_message']['id_account'],
            "id_tipo" => 'I' 
        );        
            
    } else {
        
        $params_get_outcome_groups = array (
            "id_cuenta" => $id_account,
            "id_tipo" => 'O' 
        );
        
        $params_get_income_groups = array (
            "id_cuenta" => $id_account,
            "id_tipo" => 'I' 
        );        
        
        if ($this->urldata['type'] == '') {
            $type = 'O';
             
        } else {     
            $type = $this->urldata['type'];
            
        }       
        

            
        if ($this->urldata['id_tipo'] == 'I') {
            $i_group = '';
            $o_group = ' hidden';  
        } else {
            $i_group = ' hidden';
            $o_group = '';          
        }           
    
    }    
    


    $view_data = array (
        "outcome_groups" => $wallet->getCategories($params_get_outcome_groups),
        "income_groups" => $wallet->getCategories($params_get_income_groups),
        "o_group" => $o_group,
        "i_group" => $i_group,
        "id_account" => $id_account,
        "data_movement" => $data_movement,
        "limits_account" => $wallet->checkLimitsNoPremium(),  
        "title" => "Panel de gestión"                
    );
        
    $this->renderPage('note', $view_data);
   }    
}