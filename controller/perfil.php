<?php
require("model/wallet.php");
require("model/notifications.php");

class perfil extends Controller {
    
   public function Run() {
    
    $params = array (
        "redir_not_login" => 'login',
        "rol_required" => false
    );        

    $this->accessControlPage($params);    
    
    $wallet = new wallet($this->id);
    
    $res_users = array ();    
    
    if ($_POST) { 

        // TO-DO: mejorar esto
        if ($this->urldata['action'] == 'update_profile') {
            
            $res_update_profile = $wallet->updateProfile($this->urldata);                        
            
            if ($res_update_profile['success']) {
                if ($this->urldata['premsg'] == 'fill_fields') {
                    header('Location: index.php?page=dashboard');
                    
                } else {
                    //$this->success_message = $res_update_profile['success_message'];
                    header('Location: index.php?page=perfil&success=ok');
                }
                
            } else {
                $this->danger_message = $res_update_profile['error_message'];
            }             
            
        } elseif ($this->urldata['action'] == 'search_user') {
            $res_users = $wallet->searchUsers($this->urldata['username']);
            
            
        } elseif ($this->urldata['action'] == 'add_share') {
            $params_share = array (
                "action" => 'add_share',
                "id_account" => $this->urldata['id_account'],
                "id_sharing" => $this->urldata['id_sharing']
            );            
            $res_share = $wallet->manageShare($params_share);
            
            if ($res_share['success']) {
                $this->success_message = $res_share['success_message'];
            } else {
                $this->danger_message = $res_share['error_message'];
            }            
        } elseif ($this->urldata['action'] == 'update_share') {
            $params_share = array (
                "action" => 'update_share',
                "id_account" => $this->urldata['id_account'],
                "id_sharing" => $this->urldata['id_sharing']
            );            
            $res_share = $wallet->manageShare($params_share);
            
            if ($res_share['success']) {
                $this->success_message = $res_share['success_message'];
            } else {
                $this->danger_message = $res_share['error_message'];
            }             
            
        } else {
                $res_account = $wallet->manageAccounts($this->urldata);
                
            if ($res_account['success']) {
                $this->success_message = $res_account['success_message'];
            } else {
                $this->danger_message = $res_account['error_message'];
            }
        }             
        
    }
    
    if ($this->urldata['action'] == 'remove_share') {
        $params_share = array (
            "action" => 'update_share',
            "id_account" => $this->urldata['id_account'],
            "id_sharing" => $this->urldata['id_sharing']
        );
        
        $res_share = $wallet->manageShare($params_share);
        
        if ($res_share['success']) {
            $this->success_message = $res_share['success_message'];
        } else {
            $this->danger_message = $res_share['error_message'];
        }        
    }
    
    if ($this->urldata['action'] == 'leave_account') {
        $params_share = array (
            "action" => 'leave_share',
            "id_sharing" => $this->urldata['id_sharing']
        );            
        $res_share = $wallet->manageShare($params_share);        
    }
    
    if ($this->urldata['tab'] == 'perfil-cuentas') {
        $accounts = $wallet->getUserAccountsIsOwner();
        $view = 'perfil_cuentas';
        
    } elseif ($this->urldata['tab'] == 'perfil-compartir') {
        $account_name = $wallet->getAccountNameById($this->urldata['id_account']);
        $res_users_sharing = $wallet->getUsersSharedAccount($this->urldata['id_account']);                
        
        $view = 'perfil_compartir';        
    
    } elseif ($this->urldata['tab'] == 'perfil-invitaciones') {
        $res_count_invited = $wallet->getAccountsInvited();
        $view = 'perfil_invitaciones';
        
    } elseif ($this->urldata['tab'] == 'perfil-notificaciones') {        
        
        if (!notif::isNotificationsEnabled()) {
            $aditional_class = 'hidden';
            $notifications_disabled = 1;

        } else {
            $notifications_disabled = 0;
            $aditional_class = '';

        }
        
        $notifications_type = notif::getNotificationsEnabled();
        $view = 'perfil_notificaciones';        
        
    } else {
        $view = 'perfil';
    }
    
    $data_profile = $wallet->getProfile();
    
    if ($this->urldata['premsg'] == 'fill_fields') {
        $this->danger_message = 'Completa todos los datos para acceder a la aplicaci&oacute;n';        
    }
    
    if ($this->urldata['success'] == 'ok') {
        $this->success_message = 'Datos almacenados correctamente';
        
    }
    
    $limits_account = $wallet->checkLimitsNoPremium();
    if ($limits_account['notifications']) {
        $notification_class = 'notification';
        
    } else {
        $notification_class = 'notification-no-premium';
    }   
    
    $view_data = array (
        "id_account" => $id_account,
        "accounts" => $accounts,
        "update_status" => $update_status,
        "data_profile" => $data_profile,
        "res_users" => $res_users,
        "res_users_sharing" => $res_users_sharing,
        "account_name" => $account_name,
        "res_count_invited" => $res_count_invited,
        "limits_account" => $limits_account,
        "notification" => $notifications_type,
        "notifications_disabled" => $notifications_disabled,
        "aditional_class" => $aditional_class,
        "notification_class" => $notification_class,  
        "title" => "Panel de usuario"                
    );
        
    $this->renderPage($view, $view_data);
   }    
}