<?php
require("model/wallet.php");

class premium extends Controller {
    
   public function Run() {
    
    $params = array (
        "redir_not_login" => 'login',
        "rol_required" => false
    );

    $this->accessControlPage($params);
            
    $this->renderPage('premium', $view_data);
   }
      
}