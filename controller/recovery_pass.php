<?php
class recovery_pass extends Controller {
    
   public function Run() {
    
        $to_step = 1;
        
        if ($_POST) {
            $user = new User;
            if ($this->urldata['step'] == 1) {                
                if ($user->ifUserExists($this->urldata['username'])) {
                    $to_step = 2;
                } else {
                    $to_step = 1;
                    $this->danger_message = 'Usuario no existe en el sistema';
                }            
            }
            if ($this->urldata['step'] == 2) {
                if ($user->checkSecrets($this->urldata)) {
                    $to_step = 3;
                } else {
                    $to_step = 2;
                    $this->danger_message = 'Pregunta y respuesta secreta no coinciden';
                    
                }
            }
            if ($this->urldata['step'] == 3) {
                if ($user->checkRePasswd($this->urldata)) {
                    $user->reSetPasswd($this->urldata);
                    $to_step = 4;
                } else {
                    $to_step = 3;
                    $this->danger_message = 'Claves no coinciden';
                    
                }    
            }
        }
        
        $view_data = array (
            "title" => "Recuperar contraseña"                
        );
        
        if ($to_step == 2) {
            // solicita pregunta y respuesta secreta            
            $param_secrets = array (
                'username' => $this->urldata['username']
            );
            
            $view_data['secret_question'] = $user->getSecrets($param_secrets);
            $view_data['step'] = $to_step;
            $view_data['username'] = $this->urldata['username'];
            
            // compatibilidad hacia atras            
            if ($view_data['secret_question'] == '') {
                // si no especifico pregunta secreta, tendra que solicitar soporte
                $this->renderPage('error', '');
            } else {            
                $this->renderPage('recovery_pass_step2', $view_data);
            }
            
        } elseif ($to_step == 3) {
            // pregunta y respuesta secreta son correctas
            // pedir nuevas claves
            $view_data['secret_question'] = $this->urldata['secret_question'];
            $view_data['step'] = $to_step;
            $view_data['username'] = $this->urldata['username'];
            $this->renderPage('recovery_pass_step3', $view_data);
                        
        } elseif ($to_step == 4) {
            $view_data = array (
            );            
            $this->renderPage('recovery_pass_done', $view_data);            
            
        } elseif ($to_step == 1) {
            $view_data['step'] = $step;
            $this->renderPage('recovery_pass', $view_data);
        }
   
   }    
}