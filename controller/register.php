<?php
class register extends Controller {
    
   public function Run() {
    
    $view = 'register';
    
    if ($_POST) {        
        if ($_POST['action'] == 'register') {

            $user = new User();            
            $register = $user->createUser($this->urldata);
            
            if ($register['success']) {                
                //$url = 'Location: index.php?page=register_done';
                $url = 'Location: index.php?page=perfil&premsg=fill_fields';                
                header($url);
            }  
        }
    }    

    $view_data = array (
        "title" => "Registro Wallefit",
        "register" => $register                
    );
    
    $this->renderPage($view, $view_data);
   }    
}