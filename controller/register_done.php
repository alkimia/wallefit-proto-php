<?php

class register_done extends Controller {
    
   public function Run() {
    
    $view_data = array (
        "title" => "Registro completado"                
    );
    
    $this->renderPage('register_done', $view_data);
   }    
}