<?php
require("model/wallet.php");

class timeline extends Controller {
    
   public function Run() {
    
    $params = array (
        "redir_not_login" => 'login',
        "rol_required" => false
    );

    $this->accessControlPage($params);
    
    $wallet = new wallet($this->id);
    
    if ($_GET) {
        if ($this->urldata['action'] == "delete_mov") {
            $params = array (
                "action" => "delete_movement",
                "id_account" => $this->urldata['id_account'],
                "id" => $this->urldata['mov_id']
            );
            $wallet->manageMovements($params);
        }
    } 
    
    if ($this->urldata['type'] == '') {
        $type = 'O'; 
    } else {     
        $type = $this->urldata['type'];
    }       
    
    if ($this->urldata['id_account'] == '') {
        $id_account = $wallet->getMainAccount(); 
    } else {
        $id_account = $this->urldata['id_account'];
        
    }    
    
    if ($this->urldata['mm_from'] == '') {
        $from_date = date("Y-m-01"); 
    } else {     
        $from_date = $this->urldata['yy_from']."-".$this->urldata['mm_from']."-01";
    }
    
    if ($this->urldata['mm_to'] == '') {
        $to_date = date("Y-m-31"); 
    } else {     
        $to_date = $this->urldata['yy_to']."-".$this->urldata['mm_to']."-31";
    }         
    
    $params_timeline = array (
         "id_account" => $id_account,
         "from_date" => $from_date,
         "to_date" => $to_date,
         "type" => $this->urldata['type'],
         "group_income" => $this->urldata['group_income'],
         "group_outcome" => $this->urldata['group_outcome'],
         "keyword" => $this->urldata['keyword']
    );

    $filters = array (
        "id_status" => $_GET['id_status'],
        "only_mine" => true
    );
    
    $params_get_outcome_groups = array (
        "id_cuenta" => $id_account,
        "id_tipo" => 'O' 
    );
    
    $params_get_income_groups = array (
        "id_cuenta" => $id_account,
        "id_tipo" => 'I' 
    );    

    $view_data = array (
        "title" => "Panel de gestión",
        "type" => $type,
        "from_date" => $from_date,
        "to_date" => $to_date,
        "timeline" => $wallet->getTimeLine($params_timeline),
        "outcome_groups" => $wallet->getCategories($params_get_outcome_groups),
        "income_groups" => $wallet->getCategories($params_get_income_groups),
        "id_account" => $id_account                        
    );
    
    $this->renderPage('timeline', $view_data);
   }    
}