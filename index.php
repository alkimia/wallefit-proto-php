<?php
/* MVC Simple*/
define('simplemvc_exec', 1);

if( !file_exists('base/config.php') )
{
    die('Falta archivo de configuracion');

}
else
{
   /// cargamos las constantes de configuración
   require_once('base/Controller.php');
   require_once('base/config.php');
   require_once('base/bd.php');
   require_once('base/Users.php');
   
    $banned = array(
        "INNER",
        "!--",
        "SCRIPT",
        "SELECT",
        "JOIN",
        "'");
        

    foreach($_POST as $key => $value) {
        $value = str_replace($banned, "", $value);
        
        $urldata[$key] = strip_tags ($value); 
        
        
    }
    
    foreach($_GET as $key => $value) {
        $value = str_replace($banned, "", $value);
        
        $urldata[$key] = strip_tags ($value); 
    }         
   
   /// ¿Qué controlador usar?
   $pagename = '';
   if( isset($urldata['page']) )
   {
      $pagename = $urldata['page'];
   }
   else 
   {
      $pagename = 'home';
   }
   
   if($pagename != '')
   {   
         if( file_exists('controller/'.$pagename.'.php') )
         {
            require_once 'controller/'.$pagename.'.php';
            
            try
            {
               $app = new $pagename($pagename, $urldata);
            }
            catch(Exception $e)
            {
               echo "<h1>Error fatal</h1>"
                  . "<ul>"
                       . "<li><b>Mensage:</b> Error al cargar controlador</li>"
                  . "</ul>";
            }
            
            $app->Run();
         }
         else
         {
               echo "<h1>Error fatal</h1>"
                  . "<ul>"
                       . "<li><b>Mensage:</b>No se encontró controlador</li>"
                  . "</ul>";
         }
   }
}