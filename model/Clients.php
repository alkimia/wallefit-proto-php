<?php
class Clients  {
    
    public function listaClientes() {
        $db = new Database();
        
        $sql="SELECT * FROM clientes";                
        $getRows = $db->getRows($sql);
        
        return $getRows;        
    }
    
    public function createClient($data) {
        $db = new Database();
        
        extract($data);        
        
        $sql="INSERT INTO clientes
            (nombre,email)
            VALUES
            (:nombre, :email)";

        $params = array (
            ":nombre" => $name,
            ":email" => $email
        );            
            
        $db->insertRow($sql, $params);                                                
    }    
}
?>