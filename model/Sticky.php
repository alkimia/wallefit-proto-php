<?php
class Sticky extends User {
    
    public function editSticky($params) {
        $db = new Database();
        
        extract($params); 
             
        $sql="UPDATE sticky
            SET description=:description,
            id_status=:id_status
            WHERE id_user=:id_user AND id=:id";       

        $params = array (
            ":description" => $description,
            "id_status" => $id_status,
            ":id_user" => $this->id,
            ":id" => $id
        );            
            
        $db->updateRow($sql, $params);
        
        return true;         
    }
    
    public function getItem($data) {
        $db = new Database();
                
        extract($data);
        
        $sql = "SELECT * FROM sticky WHERE id = $id AND id_user=$this->id";        
        
        return $db->getRow($sql);                 
    }
    
    public function getList($filters) {
        $db = new Database();
        
        extract($filters);
        
        $sql = "SELECT * FROM sticky WHERE 1=1";
        
        if ($id_status != '') {
            $sql.= " AND id_status = $id_status";
        }
        
        if ($only_mine) {
            $sql.= " AND id_user=$this->id";
        }    
                        
        $getRows = $db->getRows($sql);
        
        return $getRows;        
    }
    
    public function createNew($data) {
        $db = new Database();
        
        extract($data);        
        
        $sql="INSERT INTO sticky
            (id_user, description, id_status)
            VALUES
            (:id_user, :description, :id_status)";           

        $params = array (
            ":id_user" => $this->id,
            ":description" => $description,
            ":id_status" => $id_status
        );            
            
        $db->insertRow($sql, $params);
        
        return $db->last_id;                                                
    }
    
    public function deleteItem($params) {
        $db = new Database();
        extract($params);
        
        $sql = "DELETE FROM sticky WHERE id=:id AND id_user=:id_user";        
        
        $params = array (
            ":id_user" => $id_user,
            ":id" => $id
        );
        
        $db->deleteRow($sql, $params);         
    }    
}
?>