<?php
class wallet extends User {
    public function __construct($id) {
        $db = new Database();
        $this->id_user = $id;
        
        // permisos del usuario
        $sql = "SELECT premium FROM users 
        WHERE id = '$this->id_user'";        
        
        $db->atline = 23;          
        $data = $db->getRow($sql);
        
        $this->is_premium = $data['premium'];                 
    }
    
    /*
        $service
    */
    public function checkLimitsNoPremium() {
        $db = new Database();

        $max_categories_per_user = 16;
        $max_accounts_per_user = 2;
        $software_is_free = 1;                
        
/*        if ($this->is_premium || $software_is_free) {
            
            return $return;            
        }

        $sql = "SELECT count(*) AS counter 
        FROM grupos_apuntes
        WHERE id_owner = '$this->id_user'";        
        
        $db->atline = 23;          
        $data = $db->getRow($sql);
        
        if ($data['counter'] < $max_accounts_per_user) {
            $return['categories'] = 1;
            
        } else {
            $return['categories'] = 0;
            
        }                        

        $sql = "SELECT count(*) AS counter 
        FROM cuentas
        WHERE id_owner = '$this->id_user'";        
        
        $db->atline = 23;          
        $data = $db->getRow($sql);
        
        if ($data['counter'] < $max_accounts_per_user) {
            $return['accounts'] = 1;
        } else {
            $return['accounts'] = 0;
        }            
        
        $return['categories'] = 1;            
        $return['accounts'] = 1;
        $return['notifications'] = 1;*/
            $return['categories'] = 1;            
            $return['accounts'] = 1;
            $return['notifications'] = 1;
        
        
        return $return;            


    }
    
    public function getUserAccountsIsOwner() {
        $db = new Database();
        
        extract($params);
        
        $sql = "SELECT * FROM cuentas 
        WHERE id_owner='$this->id_user'";

        $db->atline = 18;          
        return $db->getRows($sql);
    }
    
    public function checkCategoryExists($params) {
        $db = new Database();
        
        extract($params);
        
        $sql = "SELECT id FROM grupos_apuntes 
        WHERE id_owner='$this->id_user'
        AND descripcion='$descripcion'
        AND id_cuenta='$id_account'
        AND tipo='$tipo'";

        $db->atline = 18;          
        $data = $db->getRow($sql);
        if ($data == '') {
            return false;
        } else {
            return true;
        }                                    
    }
    
    public function manageMovements($params) {
        $db = new Database();        
        extract($params);
        
        $params_account_permission = array (
            "id_account" => $id_account,
            "can_read" => 1,
            "id_user" => $this->id_user
        );
        
        if ($this->checkUserAccountPermissions($params_account_permission)) {
            $can_read = true;
        } else {
            $can_read = false;
        }
        
        $params_account_permission = array (
            "id_account" => $id_account,
            "id_user" => $this->id_user,
            "can_write" => 1
        );
        
        if ($this->checkUserAccountPermissions($params_account_permission)) {
            $can_write = true;
        } else {
            $can_write = false;
        }        
                
        if ($action == 'read_movement') {
            
            if ($can_read) {
            
                $sql = "SELECT * FROM apuntes WHERE id='$id_movement'";                                  
                                    
                $data = $db->getRow($sql);
                if (count($data)) {
                    $response = array();
                    $response['success'] = true;
                    $response['success_message'] = $data;                                              
                } else {
                    $response = array();
                    $response['success'] = false;
                    $response['success_message'] = 'Registro no existe o no es v�lido';                                              
                    
                }
                
                
           } else {
            
                $response = array();
                $response['success'] = false;
                $response['success_message'] = 'No tienes permisos para realizar esta operaci�n en esta cuenta';             
           }            
                
        } elseif ($action == 'delete_movement') {
            
            if ($can_write) {
            
                $sql = "DELETE FROM apuntes 
                    WHERE id_account='$id_account'
                    AND id='$id'";                                   
                    
                $db->atline = 67;             
                $db->deleteRow($sql);
            
            } else {
                
                $response = array();
                $response['success'] = false;
                $response['success_message'] = 'No tienes permisos para realizar esta operaci�n en esta cuenta';                 
            }
                                           
            
        } elseif ($action == 'add_movement' && $id_movement == '') {
            
           if ($can_write) {            
            
                if ($credit == '') {
                    $credit = 0;
                }
                if ($debit == '') {
                    $debit = 0;
                }
                
                if ($type == 'I') {
                    $credit = $amount;    
                } elseif ($type == 'O') {
                    $debit = $amount;
                }
                
                $selected_date = $yy.'-'.$mm.'-'.$dd;  
                $datetime_real = date("Y-m-d H:i:s");                           
            
                $sql="INSERT apuntes
                    (id_owner,
                    id_account,
                    datetime_selected,
                    datetime_real,
                    concept,
                    id_income_group,
                    id_outcome_group,
                    debit,
                    credit)
                    VALUES
                    ($this->id_user,
                    $id_account,
                    '$selected_date',
                    '$datetime_real',
                    '$comments',
                    '$group_income',
                    '$group_outcome',
                    $debit,
                    $credit)";                                        
                    
                $params = array();            
                 
                $db->atline = 123;                    
                $db->insertRow($sql);
                
                $response = array();
                $response['success'] = true;
                $response['success_message'] ='Apunte registrado correctamente';
            
            } else {
                header('Location: index.php?page=error');
            }
                                            
        } elseif ($action == 'add_movement' && $id_movement != '') {
            
            if ($can_write) {
            
                if ($credit == '') {
                    $credit = 0;
                }
                if ($debit == '') {
                    $debit = 0;
                }
                
                if ($type == 'I') {
                    $credit = $amount;    
                } elseif ($type == 'O') {
                    $debit = $amount;
                }
                
                $selected_date = $yy.'-'.$mm.'-'.$dd;
                
                if ($id_tipo == 'I') {
                    $credit = $amount;
                    $debit = 0;    
                } elseif ($id_tipo == 'O') {
                    $debit = $amount;
                    $credit = 0;
                }
                
                if ($group_outcome == '') {
                    $group_outcome = 0;
                }
                
                if ($group_income == '') {
                    $group_income = 0;
                }
                                                                                                            
                $sql="UPDATE apuntes
                    set
                    id_account='$id_account',
                    datetime_selected='$selected_date',
                    concept='$comments',
                    id_income_group='$group_income',
                    id_outcome_group='$group_outcome',
                    debit=$debit,
                    credit=$credit
                    WHERE
                    id='$id_movement'";
                        
                    
                    $db->atline = 180;                    
                    $db->insertRow($sql, $params);
                    
                    $response = array();
                    $response['success'] = true;
                    $response['success_message'] ='Apunte modificado correctamente';
                
            } else {
                header('Location: index.php?page=error');
            }
                                            
        }
        
        return $response;        
        
    }
    
    public function getAccountOwner($params) {
        $db = new Database();
        
        extract($params);
        
        $sql="SELECT id_owner FROM cuentas WHERE id=$id_account";
        
        $db->atline = 255;          
        $data = $db->getRow($sql);
        
        return $data['id_owner'];                        
    }
    
    public function manageCategories($params) {
        $db = new Database();
        
        extract($params);
        
        $params_account_permission = array (
            "id_account" => $id_account,
            "id_user" => $this->id_user,
            "can_read" => 1
        );
        
        if ($this->checkUserAccountPermissions($params_account_permission)) {
            $can_read = true;
        } else {
            $can_read = false;
        }
        
        $params_account_permission = array (
            "id_account" => $id_account,
            "id_user" => $this->id_user,
            "can_write" => 1
        );
        
        if ($this->checkUserAccountPermissions($params_account_permission)) {
            $can_write = true;
        } else {
            $can_write = false;
        }         
        
        if ($action == 'add_category') {
            
            if ($can_write) {
                
                $params_real_owner = array (
                    "id_account" => $id_account
                );
                $real_owner = $this->getAccountOwner($params_real_owner); 
            
                $sql = "INSERT INTO grupos_apuntes
                (id_owner,
                id_cuenta,
                descripcion,
                tipo)
                VALUES
                ('$real_owner',
                '$id_account',
                '$category_name',
                '$type')";                                                              
                            
                $db->atline = 214;                    
                $db->insertRow($sql, $params);               
                
                $response = array();
                $response['success'] = true;
                $response['success_message'] ='Categoria agregada correctamente.';
            
            } else {
                header('Location: index.php?page=error');
            }               
                        
        } 
        elseif ($action == 'rename_category') {
            
            if ($can_write) {
            
                $sql = "UPDATE grupos_apuntes
                SET descripcion='$descripcion'
                WHERE id_cuenta='$id_account'
                AND id='$id'";  
                            
                $db->atline = 204;                    
                $db->deleteRow($sql, $params);               
                
                $response = array();
                $response['success'] = true;
                $response['success_message'] ='Categoria renombrada correctamente.';
            
            } else {
                header('Location: index.php?page=error');
            }            
        }                
        else if ($action == 'delete_category') {            
           
            if ($can_write) {
            
                $sql = "DELETE FROM apuntes
                WHERE id_account='$id_account'
                AND (id_income_group='$id' OR id_outcome_group='$id')";                        
                
                $db->atline = 204;                    
                $db->deleteRow($sql, $params);                      
    
                $sql = "DELETE FROM grupos_apuntes
                WHERE id_cuenta='$id_account'
                AND id='$id'";            
                
                $db->atline = 212;                    
                $db->deleteRow($sql, $params);            
                
                $response = array();
                $response['success'] = true;
                $response['success_message'] ='Categoria elimina correctamente.';
            
            } else {
                header('Location: index.php?page=error');
            }            
                
        }
        else if ($action == 'add_category') {
            
            if ($can_write) {
            
                $params_check = array (
                    "descripcion" => $category_name,
                    "tipo" => $add_type,
                    "id_account" => $id_account
                );
                
                if ($this->checkCategoryExists($params_check)) {
                    $response = array();
                    $response['success'] = false;
                    $response['error_message'] ='Ya tenias registrada la categoria de antes';
                     
                } else {            
                    $sql="INSERT grupos_apuntes
                        (id_owner,
                        id_cuenta,
                        descripcion,
                        tipo)
                        VALUES
                        (:username,
                        :id_account,
                        :category_name,
                        :type)";                            
            
                    $params = array (
                        ":username" => $this->id_user,
                        ":id_account" => $id_account,
                        ":category_name" => $category_name,
                        ":type" => $add_type
                    );            
                    
                    $db->atline = 230;                     
                    $db->insertRow($sql, $params);
                    
                    $response = array();
                    $response['success'] = true;
                    $response['success_message'] ='Categoria registrada correctamente';                            
                }
            
            } else {
                header('Location: index.php?page=error');
                                
            }
        }
        
        return $response;        
    }
    
    public function getMainAccount($params) {
        $db = new Database();
        extract($params);
        
        $sql = "SELECT id FROM cuentas 
        WHERE id_owner=$this->id_user
        AND principal=1";                        
       
        $db->atline = 250;         
        $data = $db->getRow($sql);
        return $data['id'];                 
    }
    
    public function getCategories($params) {
        $db = new Database();
        extract($params);
        
        $sql = "SELECT ga.id, ga.descripcion, ga.system, ga.tipo 
        FROM grupos_apuntes AS ga
        LEFT JOIN cuentas_permisos AS cp ON cp.id_account = '$id_cuenta' 
        WHERE cp.to_user=$this->id_user
        AND ga.id_cuenta='$id_cuenta'
        AND ga.tipo='$id_tipo'
        ORDER BY ga.descripcion ASC"; 
        
//        echo $sql;
        
        $db->atline = 264;             
        return $db->getRows($sql);          
    }
    
    public function getCategoriesTotalDetail($params) {       
        extract($params);
        
        $params_get_categories = array (
            "id_cuenta" => $id_account,
            "id_user" => $this->id_user,
            "id_tipo" => $type
        );
        
        $list_categories = $this->getCategories($params_get_categories);
        
        if ($type == 'I') {
            $field = 'credit';
        } elseif ($type == 'O') {
            $field = 'debit';
        }
        
        foreach($list_categories as $item_categories) {            
            
            $params_get_movements = array(
                'field' => $field,
                'id_category' => $item_categories['id'],
                'id_account' => $id_account,
                'month' => $month,
                'year' => $year,
                'range' => 'same'
            );
            
            $total_movement = $this->getTotalMovements($params_get_movements);            
            
            $line = array (
                "total" => $total_movement,
                "name" => $item_categories['descripcion'],
                "system" => $item_categories['system'],
                "type" => $item_categories['tipo'],
                "id" => $item_categories['id']
            );
            
            $categories[] = $line;    
        }
        
        arsort($categories);        
        
        return $categories;
    }
    
    public function getBalanceLastMonth($params) {
        extract($params);                
        
        $params_balance = array (
            "to_date" => $year . "-" . $month . "-01",
            "id_account" => $id_account
        );
        
        return $this->getBalance($params_balance);
        
    }
    
    public function getBalanceCurrentMonth($params) {
        extract($params);                
        
        $params_balance = array (
            "from_date" => $year . "-" . $month . "-01",
            "to_date" => $year . "-" . $month . "-31",
            "id_account" => $id_account
        );
        
        return $this->getBalance($params_balance);        
    }
    
    public function getBalance($params) {
        $db = new Database();
        extract($params);
        
        $sql = "SELECT sum(credit)-sum(debit) as balance
        FROM apuntes AS a
        LEFT JOIN cuentas_permisos AS cp ON cp.id_account = a.id_account                 
        WHERE cp.to_user=$this->id_user
        AND a.id_account='$id_account'";
        
        if ($from_date != '' && $to_date != '') {        
            $sql.=" AND a.datetime_selected BETWEEN '$from_date' AND '$to_date'";
        } elseif ($to_date !='' ) {
            $sql.=" AND a.datetime_selected<'$to_date'";
        }                                                
        
        $db->atline = 345;                     
        $data = $db->getRow($sql);
        
        if ($data['balance'] == 0) {
            return 0;
        } else {
            return $data['balance'];
        }         
    }
    
    public function getTotalMovements($params) {
        $db = new Database();
        extract($params);
        
        if ($range == "same") {
            $from_date = $year . "-" . $month . "-01";
            $to_date = $year . "-" . $month . "-31";
        }        
        
        $sql = "SELECT sum($field) as total 
        FROM apuntes AS a         
        LEFT JOIN cuentas_permisos AS cp ON cp.id_account = a.id_account                 
        WHERE cp.to_user=$this->id_user
        AND a.id_account='$id_account'";
        
        if ($id_category != '') {
            $sql.=" AND (a.id_income_group='$id_category' OR a.id_outcome_group='$id_category')";
        }
        
        if ($from_date != '' && $to_date != '') {        
            $sql.=" AND a.datetime_selected BETWEEN '$from_date' AND '$to_date'";
        } elseif ($to_date !='' ) {
            $sql.=" AND a.datetime_selected<'$to_date'";
        }                                             
        
        $db->atline = 374;                             
        $data = $db->getRow($sql);
        
        if ($data['total'] == 0) {
            return 0;
        } else {
            return $data['total'];
        }          
    }
    
    public function getUserAccounts($params) {
        $db = new Database();
        extract($data);
        
        $sql = "SELECT cp.id_account as id, cp.id_owner, u.username, c.descripcion 
        FROM cuentas_permisos AS cp
        LEFT JOIN cuentas AS c ON c.id = cp.id_account
        LEFT JOIN users AS u ON u.id = cp.id_owner 
        WHERE cp.to_user = $this->id_user";
        
        //echo $sql;       
        
        $db->atline = 462;                             
        return $db->getRows($sql);                          
    }
    
    public function getCategoryNameById($params) {
        $db = new Database();
        extract($params);
        
        $sql = "SELECT descripcion FROM grupos_apuntes AS ga
        LEFT JOIN cuentas_permisos AS cp ON cp.id_account=ga.id_cuenta 
        WHERE ga.id='$id_category'
        AND ga.id_cuenta='$id_account'
        AND cp.to_user=$this->id_user";        
        
        $db->atline = 478;                             
        $data = $db->getRow($sql);
        
        return $data['descripcion'];            
    }
    
    public function getTimeLine($params) {
        $db = new Database();
        extract($params);
        
        $params_account_permission = array (
            "id_account" => $id_account,
            "id_user" => $this->id_user,
            "can_read" => 1
        );
        
        if (!$this->checkUserAccountPermissions($params_account_permission)) {
            header('Location: index.php?page=error');
        }
        
        if ($init == '') { $init = 0; }
        if ($limit == '') { $limit = 20; }
        
        if ($group_outcome != '') {
            $id_grupo = $group_outcome;
        }
        
        if ($group_income != '') {
            $id_grupo = $group_income;
        }
        
        // balance del mes anteior como punto de partida
        $params_last_month = array(
            "to_date" => $from_date,
            "id_account" => $id_account        
        );    
        
        $balance_last_month = $this->getBalance($params_last_month);
                
        // ahora, timeline, en orden inverso para poder hacer calculos correctamente
        $sql = "SELECT a.id as id, a.credit as credit, a.debit as debit, a.concept as concept, a.datetime_selected as datetime_selected, a.id_income_group as id_income_group, a.id_outcome_group as id_outcome_group    
        FROM apuntes AS a 
        LEFT JOIN cuentas_permisos AS cp ON cp.id_account = a.id_account 
        WHERE cp.to_user='$this->id_user'
        AND a.id_account='$id_account'
        AND a.datetime_selected BETWEEN '$from_date' AND '$to_date'";
        
        
        if ($id_grupo != "") {
            $sql.=" AND (a.id_income_group='$id_grupo' OR a.id_outcome_group='$id_grupo')";    
        } else {
            if ($type == 'I') {
                $sql.=" AND a.id_income_group!=0";
            } elseif ($type == 'O') {
                $sql.=" AND a.id_outcome_group!=0";
            }
        }
        
        if ($keyword != "") {
            $sql.=" AND a.concept LIKE '%$keyword%'";    
        }    
            
        
        $sql.=" ORDER BY a.datetime_selected ASC";
        
        //echo $sql;                            
        
        $db->atline = 416;           
        $raw_timeline = $db->getRows($sql);                
        
        foreach($raw_timeline as $item_line) {
            
            if ($item_line['credit'] != 0) {
                $balance_last_month = $balance_last_month + $item_line['credit'];
                $category_selected = $item_line['id_income_group'];                
            } else {
                $balance_last_month = $balance_last_month - $item_line['debit'];
                $category_selected = $item_line['id_outcome_group'];
            }
            
            $lines = '';
            $lines = array(
                "id" => $item_line['id'],
                "credit" => $item_line['credit'],
                "debit" => $item_line['debit'],
                "balance" => $balance_last_month, 
                "concept" => $item_line['concept'],
                "date" => $item_line['datetime_selected'],
                "category_description" => $this->getCategoryNameById(array ("id_account" => $id_account, "id_category" => $category_selected))
            );
            
            
            $result_timeline[] = $lines;
        }
        
        return array_reverse($result_timeline);                         
    }
        
    public function manageAccounts($params) {
        $db = new Database();
        extract($params);
        
        
        if ($action_fa == 'add_account') {
                
            $sql = "INSERT INTO cuentas
            (id_owner,
            descripcion)
            VALUES
            ($this->id_user,
            '$name_account')";                          
                        
            $db->atline = 563;                    
            $db->insertRow($sql, $params);
            
            $id_account = $db->last_id;             
            
            // crear categorias de gasto e ingreso
            $id_owner = $this->id_user;
            
            $categorias_gasto = array (
                'Comida' => 'O', 
                'Coche' => 'O',
                'Ocio' => 'O',
                'Farmacia' => 'O', 
                'Telef�nia' => 'O', 
                'Casa' => 'O',
                'Ropa' => 'O',
                'Transporte' => 'O',
                'Electricidad - Gas' => 'O',
                'Ahorros' => 'I',
                'Financieros' => 'I',
                'Sueldo' => 'I',
                'Pensiones' => 'I'
            );
            
            foreach($categorias_gasto as $key => $value) {
                
                
                $keyname = utf8_encode ($key);
                
                $sql = "INSERT INTO grupos_apuntes
                (id_owner, 
                id_cuenta,
                descripcion,
                tipo,
                system)
                VALUES
                ('$id_owner',
                '$id_account',
                '$keyname',
                '$value',
                1)";
                
                $db->atline = 81;
                $db->insertRow($sql);                                   
                
            }
                           
            
            $response = array();
            $response['success'] = true;
            $response['success_message'] ='Cuenta agregada correctamente.';
            
            return $response;
        
        } elseif ($action_sel == 'sel_account') {
                        
            $params_account_permission = array (
                "id_account" => $account_selected,
                "id_user" => $this->id_user,
                "can_read" => 1
            );
            
            if ($this->checkUserAccountPermissions($params_account_permission)) {
                $can_read = true;
            } else {
                $can_read = false;
            }
            
            if ($can_read) {
            
                // deseleccionar las anteriores
                $sql = "UPDATE cuentas
                SET principal=0
                WHERE id_owner=$this->id_user";                         
                            
                $db->atline = 582;                    
                $db->insertRow($sql, $params);
                
                // marcar la nueva            
                
                $sql = "UPDATE cuentas
                SET principal=1
                WHERE id='$account_selected'
                AND id_owner=$this->id_user";                         
                            
                $db->atline = 592;                    
                $db->insertRow($sql, $params);               
                
                $response = array();
                $response['success'] = true;
                $response['success_message'] ='Cuenta seleccionada correctamente.';
                
                return $response;
            
            } else {
                header('Location: index.php?page=error');                
            }
            
        } elseif ($action_fa == 'delete_account') {
            
            $params_account_permission = array (
                "id_account" => $account_id,
                "id_user" => $this->id_user,
                "can_write" => 1
            );            
            
            if ($this->checkUserAccountPermissions($params_account_permission)) {
                $can_write = true;
            } else {
                $can_write = false;
            }            
            
            if ($can_write) {
            
                // eliminar registros de la cuenta
                $sql = "DELETE FROM apuntes WHERE 
                id_account = '$account_id'
                AND id_owner = $this->id_user";
    
                $db->atline = 608;             
                $db->deleteRow($sql);
                
                // eliminar cuenta
                $sql = "DELETE FROM grupos_apuntes WHERE 
                id_cuenta = '$account_id'
                AND id_owner = $this->id_user";
               
                // eliminar cuenta
                $db->atline = 617;              
                $db->deleteRow($sql);
                
                $sql = "DELETE FROM cuentas WHERE 
                id = '$account_id'
                AND id_owner = $this->id_user";
                
                $db->atline = 624;             
                $db->deleteRow($sql);
                
                // eliminar permisos
                $sql = "DELETE FROM cuentas_permisos WHERE 
                id_account = '$account_id'
                AND id_owner = $this->id_user";                                                 
                
                $response = array();
                $response['success'] = true;
                $response['success_message'] ='Cuenta eliminada correctamente.';
            
            } else {
                header('Location: index.php?page=error');
                                
            }
                        
        } elseif ($action_fa == 'rename_account') {
            
            $params_account_permission = array (
                "id_account" => $account_id,
                "id_user" => $this->id_user,
                "can_write" => 1
            );
            
            if ($this->checkUserAccountPermissions($params_account_permission)) {
                $can_write = true;
            } else {
                $can_write = false;
            }              
            
            if ($can_write) {

                $sql = "UPDATE cuentas 
                SET descripcion = '$name_account'
                WHERE id_owner = $this->id_user
                AND id = '$account_id'";
                
                $response = array();
                $response['success'] = true;
                $response['success_message'] ='Cuenta modificada correctamente.';             
    
                $db->atline = 638;             
                $db->deleteRow($sql);
            } else {
                header('Location: index.php?page=error');
                                
            }            
        }
        
        return $response;        
    }
    
    public function searchUsers($search) {
        $db = new Database();
        
        if ($search != '') {

            $sql = "SELECT * FROM users
            WHERE username LIKE '%$search%'";
                                
            $db->atline = 699;          
            return $db->getRows($sql);
        
        }      
    }
    
    public function getAccountNameById($id_account) {
        $db = new Database();

        $sql = "SELECT descripcion FROM cuentas
        WHERE id='$id_account'
        AND id_owner=$this->id_user";
                            
        $db->atline = 714;          
        $data = $db->getRow($sql);
        
        if ($data == '') {
            // no existe, o no es la suya
            header('Location: index.php?page=error');
        } else {        
            return $data['descripcion'];
        }         
    }
    
    public function checkUserAccountPermissions($params) {
        extract($params);
        
        $db = new Database();

        $sql = "SELECT * FROM cuentas_permisos
        WHERE id_account='$id_account'
        AND to_user=$id_user";
                                
        
        $db->atline = 733;          
        $data = $db->getRow($sql);        
        
        $return = false;
        
        if ($data['can_r'] && $can_read) {
            $return = true;
        } 
                                
        if ($data['can_w'] && $can_write) {
            $return = true;
        }
            
        return $return;
    }
    
/*    public function checkUserAccountPermissions($id_account) {
        $db = new Database();
                
        // comprobar si el usuario tiene permisos sobre la cuenta
        $sql = "SELECT * FROM cuentas_permisos
        WHERE id_account = $id_account
        AND to_user = $this->id_user";
        $db->atline = 768;          
        $data = $db->getRow($sql);
        
        if (count($data) > 0) {
            // no existe, o no es la suya
           return true;
            
        } else {
            return false;
        }
    }  */  
    
    public function manageShare($params) {
        $db = new Database();
        extract($params);
        
        $params_account_permission = array (
            "id_account" => $id_account,
            "id_user" => $this->id_user,
            "can_read" => 1
        );
        
        if ($this->checkUserAccountPermissions($params_account_permission)) {
            $can_read = true;
        } else {
            $can_read = false;
        }
        
        $params_account_permission = array (
            "id_account" => $id_account,
            "id_user" => $this->id_user,
            "can_write" => 1
        );
        
        if ($this->checkUserAccountPermissions($params_account_permission)) {
            $can_write = true;
        } else {
            $can_write = false;
        }          
        
        
        if ($action == 'leave_share') {
            $sql = "DELETE FROM cuentas_permisos
            WHERE id='$id_sharing'
            AND to_user=$this->id_user";
            
            $db->deleteRow($sql);
            
            $response = array();
            $response['success'] = true;
            $response['success_message'] ='Ha abandonado correctamente la cuenta.';
                        
        } elseif ($action == 'remove_share') {
            
            if ($can_write) {
            
                $sql = "DELETE FROM cuentas_permisos
                WHERE id='$id_sharing'
                AND id_account='$id_account'
                AND id_owner=$this->id_user";
                
                $db->deleteRow($sql);
                
                $response = array();
                $response['success'] = true;
                $response['success_message'] ='Permisos sobre la cuenta eliminados correctamente.';
            
            } else {
                header('Location: index.php?page=error');
            }
            
        } elseif ($action == 'update_share') {
            
            if ($can_write) {
            
                // eliminar permisos anteriores            
                $sql = "DELETE FROM cuentas_permisos
                WHERE id_account='$id_account'
                AND id_owner='$this->id_user'
                AND to_user!=$this->id_user";
                
                $db->deleteRow($sql);
                
            } else {
                header('Location: index.php?page=error');
            }
                        
        }
        
        if ($can_write) {
        
            if ($action == 'add_share' || $action == 'update_share') {
                if ($_POST['permissions']) {            
                    foreach($_POST['permissions'] as $key => $value) {
                        
                        if ($value != '') {
            
                            $can_r = 1;
                            
                            if ($value == 'w') {
                                $can_w = 1;
                            } else {
                                $can_w = 0;
                            }
                            
                            $sql = "INSERT INTO cuentas_permisos
                            (id_account,
                            id_owner,
                            to_user,
                            can_r,
                            can_w)
                            VALUES
                            ('$id_account',
                            $this->id_user,
                            '$key',
                            '$can_r',
                            '$can_w')";            
                            
                            $db->atline = 781;                    
                            $db->insertRow($sql);
                        
                        }                            
                        
                    }
                }
            } 
        
        }
        
        $response = array();
        $response['success'] = true;
        $response['success_message'] ='Permisos sobre la cuenta agregados correctamente.';
        
        
        return $response;                               
    }
    
    public function getUsersSharedAccount($id_account) {
        $db = new Database();
        
        $sql = "SELECT c.to_user, c.can_r, c.can_w, u.username, u.description, c.id as id_permission
            FROM cuentas_permisos as c 
            LEFT JOIN users as u ON u.id=c.to_user
            WHERE c.id_account='$id_account'
            AND c.id_owner='$this->id_user'
            AND c.to_user!=$this->id_user";                        
            
        $db->atline = 846;          
        return $db->getRows($sql);        
    }
    
    public function getAccountsInvited() {
        $db = new Database();
        
        $sql = "SELECT c.descripcion, cp.id as id_permission, u.username as owner  
            FROM cuentas_permisos AS cp
            LEFT JOIN cuentas AS c ON c.id = cp.id_account
            LEFT JOIN users AS u ON u.id = cp.id_owner
            WHERE to_user='$this->id_user'
            AND c.id_owner!=$this->id_user";                                    
            
        $db->atline = 860;          
        return $db->getRows($sql);          
    }        
}
?>