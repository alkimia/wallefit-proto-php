$( document ).ready(function() {
        
    $('.create-account').click(function() {
        location.href='index.php?page=perfil&tab=perfil-cuentas';
    });
    
    $('.permissions-acount').click(function() {
        var id_account = $('#id_account').val();
        location.href='index.php?page=perfil&tab=perfil-compartir&id_account=' + id_account;
    }); 
    
    $('.btn-news').click(function() {
        location.href='index.php?page=news';
    });    
    
               
        
    $("#type").change(function() {
        $("#footer").removeClass('hidden');
        
        var id_tipo = $(this).val();
        if (id_tipo == 'I') {
            console.log('I');
            $(".O_group").addClass('hidden');
            $(".I_group").removeClass('hidden');
            
        } else if (id_tipo == 'O') {
            console.log('O');
            $(".I_group").addClass('hidden');
            $(".O_group").removeClass('hidden');
            
        }
        
        $("#footer").addClass('hidden');
    });
    
    $('.edit-category').click(function() {
        var category_name = prompt("�Que nombre le ponemos a esta categoria?", "");
        
        if (category_name != null) {
            $("#footer").removeClass('hidden');
            
            var cat_id = $(this).data('id');
            
            $("#action").val('rename_cat');
            $("#cat_id").val(cat_id);
            $("#name_cat").val(category_name);
            document.form_actions.submit();            
        }
    });
    
    $('.add-category').click(function() {
        var category_name = prompt("�Que nombre le ponemos a la nueva categoria?", "");
        console.log(category_name);
        
        if (category_name == '' || category_name == null) {
            alert('Falta indicar nombre para la categoria');
            
        } else {
            $("#footer").removeClass('hidden');
            
            $("#action").val('add_cat');
            $("#name_cat").val(category_name);
            $("#type_sel").val($("#type").val());

            document.form_actions.submit();
        }
    });    
    
    $('.delete-category-nope').click(function() {
        alert('Solo puedes cambiar el nombre a esta categoria, no borrarla.');
    });                  
    
    $('.delete-category').click(function() {
        if(confirm("Vas a eliminar la categoria, esto no se puede deshacer.\nEst�s segur@?")) {
            $("#footer").removeClass('hidden');
            
            var cat_id = $(this).data('id');
            
            $("#action").val('delete_cat');
            $("#cat_id").val(cat_id)
            document.form_actions.submit();           
        }        
    });

    $('.btn-new-outcome').click(function() {
        $("#footer").removeClass('hidden');
        
        var account = $("#id_account").val();
        location.href='index.php?page=note&type=O&id_account=' + account;
    });
    
    $('.btn-new-income').click(function() {
        $("#footer").removeClass('hidden');
        
        var account = $("#id_account").val();
        location.href='index.php?page=note&type=I&id_account=' + account;
    });    

    $('.view-month-detail').click(function() {
        $("#footer").removeClass('hidden');
        
        var account = $("#id_account").val();
        location.href='index.php?page=timeline&id_account=' + account;
    });
    
    $('#id_account').change(function() {
        $("#footer").removeClass('hidden');
        
        var id_account = $('#id_account').val();
        location.href='index.php?page=dashboard&id_account=' + id_account;
    });
    
    $('.on-card').change(function() {
        $("#footer").removeClass('hidden');
        
        var id_account = $('#id_account').val();
        location.href='index.php?page=timeline&id_account=' + id_account;
    });  
    
    $('.link_to_perfil').click(function() {
        $("#footer").removeClass('hidden');
        
        location.href='index.php?page=perfil';
    });
    
    $('.filter-year-month').click(function() {
        $("#footer").removeClass('hidden');
        
        document.form_year_month.submit();
    });
    
    $('.view-premium').click(function() {
        $("#footer").removeClass('hidden');
        
        location.href='index.php?page=premium';
    });            
});