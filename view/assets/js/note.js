$( document ).ready(function() {
    
    $('.go-home').click(function() {
        location.href='index.php?page=dashboard';
    });
    
    $('#id_account').change(function() {
        var id_account = $("#id_account").val();
        var id_tipo = $("#type").val();
        location.href="index.php?page=note&type=" + id_tipo + "&id_account=" + id_account;        
    });        
    
    $("#type").change(function() {
        var id_tipo = $(this).val();
        if (id_tipo == 'I') {
            console.log('I');
            $(".O_group").addClass('hidden');
            $(".I_group").removeClass('hidden');
            $("#add_type").val('I');
            
        } else if (id_tipo == 'O') {
            console.log('O');
            $(".I_group").addClass('hidden');
            $(".O_group").removeClass('hidden');
            $("#add_type").val('O');
            
        }
    });
    
    $('.selector-income').change(function() {
        enableAddCat($(this).val());
    });

    $('.selector-outcome').change(function() {
        enableAddCat($(this).val());
    });
    
    $('.btn-save-note').click(function() { 
        var amount = $("#amount").val();
        var comment = $("#comments").val();
        var type = $("#id_tipo").val();
        var outcome_group = $("#group_outcome").val(); 
        var income_group = $("#group_income").val();
        
        if (amount == '' 
        || comment == ''
        || type == ''
        || (outcome_group == '' && income_group == '')) {
            alert('Revisa el formulario, te faltan datos');
        } else {
            document.form_add_movement.submit();
        }
            
    });
    
    $('.btn-calculator').click(function() {
        var amount_sel = $(this).data('calculator');
        var actual_amount = $("#amount").val();
        
        $("#amount").val( actual_amount + amount_sel);        
    });
    
    $('.btn-calculator-clear').click(function() {
        $("#amount").val('');
    });
    
    $('.btn-retro').click(function() {
        var actual_amount = $("#amount").val();
        
        $("#amount").val( actual_amount.substring(0, actual_amount.length - 1) ); 
    });
    
    function enableAddCat(id) {
        if (id == -1) {
            $('.add_category').removeClass('hidden');
        } else {
            $('.add_category').addClass('hidden');
        }
    }
    
   $("#amount").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    
    $('.link_to_perfil').click(function() {
        $("#footer").removeClass('hidden');
        
        location.href='index.php?page=perfil';
    });
    
   $('.btn-send-category').click(function() {
        var category_name = $("#category_name").val();
        
        if (category_name == '' ) {
            alert('Falta indicar nombre para la categoria');
        } else {
                    $("#footer").removeClass('hidden');
            document.form_add_category.submit();
        }

    });              

    $('.view-premium').click(function() {
        $("#footer").removeClass('hidden');
        
        location.href='index.php?page=premium';
    }); 
    
});