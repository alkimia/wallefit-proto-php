$( document ).ready(function() {
    $('.btn-my-accounts').click(function() {
        $("#footer").removeClass('hidden');
        location.href='index.php?page=perfil&tab=perfil-cuentas';
    });
    
    $('.btn-my-settings').click(function() {
        $("#footer").removeClass('hidden');
        location.href='index.php?page=perfil';
    });
     
    $('.btn-invited-accounts').click(function() {
        $("#footer").removeClass('hidden');
        location.href='index.php?page=perfil&tab=perfil-invitaciones';
    });
    
    $('.btn-my-notifications').click(function() {
        $("#footer").removeClass('hidden');
        location.href='index.php?page=perfil&tab=perfil-notificaciones';
    });        
        
    $('.delete-account-nope').click(function() {
        alert('Solo puedes cambiar el nombre a esta cuenta, no borrarla.');
    }); 
    
    $('.delete-account').click(function() {
        if(confirm("Vas a ELIMINAR esta cuenta, esto no se puede deshacer.\nEstás segur@?")) {
            var account_id = $(this).data('id');
            
            $("#footer").removeClass('hidden');
            
            $("#action_fa").val('delete_account');
            $("#account_id").val(account_id);
            document.form_actions.submit();           
        }         
    });
    
    $('.btn-add-account').click(function() {
        var account_name = prompt("¿Que nombre le ponemos a la nueva cuenta?", "");
        
        if (account_name != null) {
            
            $("#action_fa").val('add_account');
            $("#name_account").val(account_name);
            document.form_actions.submit();            
        }
    });
    
    $('.delete-account-nope-main').click(function() { 
        alert('Para borrar esta cuenta tienes que marcar otra como principal');
    });
    
    $('.edit-account-name').click(function() {
        
        var account_name = prompt("¿Que nombre le ponemos a esta cuenta?", "");
        
        if (account_name != null) {
            var account_id = $(this).data('id');
            
            $("#action_fa").val('rename_account');
            $("#account_id").val(account_id);
            $("#name_account").val(account_name);
            document.form_actions.submit();             
        }            
    });
    
    $('.share-account').click(function() {
        var account_id = $(this).data('id');
        $("#footer").removeClass('hidden');
        location.href="index.php?page=perfil&tab=perfil-compartir&id_account=" + account_id;    
    });        
    
    $('.sharing-off').click(function() {
        var id_sharing = $(this).data('id');
        var id_account = $("#id_account").val();
        location.href = "index.php?page=perfil&tab=perfil-compartir&id_account=" + id_account + "&action=remove_share&id_sharing=" + id_sharing;
    });
    
    $('.exit-account').click(function() {
        if(confirm("Vas a SALIR de esta cuenta, esto no se puede deshacer.\nEstás segur@?")) {
            var id_account = $(this).data('id');
            location.href = "index.php?page=perfil&tab=perfil-invitaciones&action=leave_account&id_sharing=" + id_account;
        }
    });
    
    $('.link_to_perfil').click(function() {
        $("#footer").removeClass('hidden');
        
        location.href='index.php?page=perfil';
    });   
    
    $('.btn-save-profile').click(function() {
        var form_ok = true;
        
        var pass = $('#pass').val();
        var repass = $('#repass').val();
        
        if (pass != '' && repass != '') {
            if (pass != repass) {
                form_ok = false;
                alert('No coincide la clave');
                    
            } else {
                if (pass.length < 5) {
                    alert('Clave demasiado corta');
                    form_ok = false;
                }
            }
        }
        
        if (form_ok) {
            $("#footer").removeClass('hidden');
            document.form_data_profile.submit();
        }
    });
    
    $('.view-premium').click(function() {
        $("#footer").removeClass('hidden');
        
        location.href='index.php?page=premium';
    });
    
    $('.disable-all-notif').click(function() {
        var status_notif = $(this).is(":checked");
        var id_owner = $("#id_owner").val();
        
        if (status_notif == true) {
            $('.notif_option').addClass('hidden');
            
        } else {
            $('.notif_option').removeClass('hidden');
        }
        
       var params = {
                action: 'update_recv_notifications',
                id_owner: id_owner,
                last_state: !status_notif
        }        
        
        $.ajax({
            url: 'ajax/api_ajax.php',
            type: 'POST',
            data : params,
            cache: false,
            success: function(response) {
                if (response) {
                    //
                    try {
                        Android.showAlert('Datos almacenados correctamene');
                    } catch(err) {
                        alert('Datos almacenados correctamente');
                    }
                }
            }
        });        
    });
    
    $('.notification').click(function() {
        var notif_id = $(this).data('id');
        var status_notif = $(this).is(":checked");        
        var id_owner = $("#id_owner").val();
        
        $("#footer").removeClass('hidden');
        
       var params = {
                action: 'update_notifications',
                id_owner: id_owner,
                id_notification: notif_id,
                last_state: status_notif
        }        
        
        $.ajax({
            url: 'ajax/api_ajax.php',
            type: 'POST',
            data : params,
            cache: false,
            success: function(response) {
                if (response) {
                    //
                    try {
                        Android.showAlert("Datos almacenados correctamente");
                    } catch(err) {
                        alert("Datos almacenados correctamente");
                    }
                    $("#footer").addClass('hidden');
                }
            }
        });        
    });
    
    $('.notification-no-premium').click(function() {
        location.href='index.php?page=premium';  
    });    
    
});