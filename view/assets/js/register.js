$( document ).ready(function() {
    $('.do-register').click(function() {
        var register_ok = true; 
               
        var username_length = $('#name').val().length;
        var pass_length = $('#pass').val().length;
        
        console.log(pass_length);
        
        if (username_length < 6) {
            alert('El usuario debe contener minimo 6 caracteres');
            register_ok = false;
        }       

        if (pass_length < 6) {
            alert('La clave debe contener minimo 6 caracteres\nLas claves no son para recordarlas, son para proteger tu cuenta');
            register_ok = false;
        }
        
        if (register_ok) {
            document.form_register.submit();
        }       

        
    });
    
    $('.go-login').click(function() {     
        location.href='index.php?page=login';
    });
    
   $('.username-register').on("keydown", function(event){
      // Allow controls such as backspace
      var arr = [8,16,17,20,35,36,37,38,39,40,45,46,189,190];
    
      // Allow letters
      for(var i = 65; i <= 90; i++){
        arr.push(i);
      }
    
      // Prevent default if not in array
      if(jQuery.inArray(event.which, arr) === -1){
        event.preventDefault();
      }
    });
});