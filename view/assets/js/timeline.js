$( document ).ready(function() {    
    $('.delete-note').click(function() {
        if(confirm("Estás segur@?")) {
            var mov_id = $(this).data('id');
            
            $("#action").val('delete_mov');
            $("#mov_id").val(mov_id)
            document.filter_detail.submit();           
        }
    });
    
    $('#id_account').change(function() {
        $("#footer").removeClass('hidden');
        
        var id_account = $('#id_account').val();
        location.href='index.php?page=timeline&id_account=' + id_account;
    });      
    
    $("#id_account").change(function() {
        var id_account = $("#id_account").val();
        $("#id_account_sel").val(id_account);
    });         
    
    $("#type").change(function() {
        var id_tipo = $(this).val();
        if (id_tipo == 'I') {
            $(".O_group").addClass('hidden');
            $(".I_group").removeClass('hidden');
            $("#add_type").val('I');
            
        } else if (id_tipo == 'O') {
            $(".I_group").addClass('hidden');
            $(".O_group").removeClass('hidden');
            $("#add_type").val('O');
            
        } else {
            $(".O_group").addClass('hidden');
            $(".I_group").addClass('hidden');
            $("#add_type").val('');
            
        }
    }); 
    
    $('.edit-note').click(function() {
            var mov_id = $(this).data('id');
            location.href='index.php?page=note&id_movement=' + mov_id;
    }); 
    
    $('.link_to_perfil').click(function() {
        $("#footer").removeClass('hidden');
        
        location.href='index.php?page=perfil';
    });            
});