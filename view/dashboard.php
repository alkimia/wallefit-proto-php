<?php
if (!defined('simplemvc_exec')) {
    die('No tiene acceso directo a este recurso');
}

extract($view_data);

$this->Header($view_data);
$this->addScripts('dashboard.js');
        
?>
    <?php echo $this->whoami(); ?>
    <?php echo $this->latest(); ?>
    <?php
        $params = array (
            "add_buttons" => true,
            "limits_account" => $limits_account
        );  
        echo $this->accountFilter($params); 
    ?>
          
    <h4>Resumen</h4>
    
    <?php echo $this->messagesBox(); ?>    
    
        <form name="form_actions" method="get" action="">
            <input type="hidden" name="action" id="action">
            <input type="hidden" name="cat_id" id="cat_id">
            <input type="hidden" name="page" value="dashboard">
            <input type="hidden" name="name_cat" id="name_cat">
            <input type="hidden" name="id_account" value="<?php echo $id_account; ?>">
            <input type="hidden" name="type_sel" id="type_sel">
        </form>
        
        
        <form method="get" action="" name="form_year_month">
            <div class="row">
                <div class="col-md-7">
                    <select name="month_selected" class="form-control">
                    <?php
                    $meses = array (
                        "1" => "Enero",
                        "2" => "Febrero",
                        "3" => "Marzo",
                        "4" => "Abril",
                        "5" => "Mayo",
                        "6" => "Junio",
                        "7" => "Julio",
                        "8" => "Agosto",
                        "9" => "Septiembre",
                        "10" => "Octubre",
                        "11" => "Noviembre",
                        "12" => "Diciembre"
                    );
                    for($i = 1; $i < 13; ++$i) { ?>
                        <option value="<?php echo $i; ?>" <?php if ($this->urldata['month_selected'] == $i) { echo ' selected'; } ?>><?php echo $meses[$i]; ?></option>
<?php } ?>                    
                    </select>
                </div>    
                <div class="col-md-3">
                    <select name="year_selected" class="form-control">
<?php
                    $year_init = date("Y") - 2;
                    $year_end = date("Y") + 1;
                    for($i = $year_init ; $i < $year_end; ++$i) { ?>
                        <option value="<?php echo $i; ?>" <?php if ($this->urldata['year_selected'] == $i) { echo ' selected'; } ?>><?php echo $i; ?></option>
                    <?php } ?>                    
                    </select>
                </div>   
                                 
                <div class="col-md-2 col-xs-12 col-sm-2 text-center" style="padding-top: 10px;">
                    <button type="button" class="btn btn-primary btn-block filter-year-month">Ver</button>
                </div>
            </div>
            <input type="hidden" name="page" value="dashboard">
            <input type="hidden" name="id_account" value="<?php echo $id_account; ?>">                
        </form>
        
        <div style="clear: both;"></div>
        <div style="height: 20px;"></div>
        <div style="clear: both;"></div>
        
        <table class="table">
        <tbody>
          <!-- Aplicadas en las filas -->
          <tr>
            <td>Saldo mes anterior</td>
            <td align="right"><b><?php echo number_format($balance_last_month,2,',','.'); ?> &euro;</b></td>
          </tr>          
          <tr class="warning">
            <td>Saldo mes</td>
            <td  align="right"><?php echo number_format($balance_this_month,2,',','.'); ?> &euro;</td>
          </tr>
          <tr class="success">
            <td>Ingresos</td>
            <td align="right">+ <?php echo number_format($total_credit,2,',','.'); ?> &euro;</td>          
          
          </tr>
          <tr class="danger">
            <td>Gastos</td>
            <td align="right">- <?php echo number_format($total_debit,2,',','.'); ?> &euro;</td>                   
          </tr>
         
          <!-- Aplicadas en las celdas (<td> o <th>) -->
        </tbody>
        </table>
        
        <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <button type="button" class="btn btn-info btn-block view-month-detail">Ver detalle mes</button>
                </div>
        </div>
        
        <div style="clear: both;"></div>
        <div style="height: 20px;"></div>
        <div style="clear: both;"></div>        
        
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                    <label>Ver tipo movimiento</label>
                    <select name="type" id="type" class="form-control">
                        <option value="O" <?php if ($this->urldata['type']=='O') { echo ' selected'; } ?>>Gasto</option>                    
                        <option value="I" <?php if ($this->urldata['type']=='I') { echo ' selected'; } ?>>Ingreso</option>
                    </select>
            </div>                        
        </div>       
        
        <h4>Categorias de gasto</h4>
        
        <button type="button" class="btn btn-success  btn-block btn-new-outcome"><i class="fa fa-plus-circle" aria-hidden="true"></i> Nuevo apunte</button>
        
        <div style="clear: both;"></div>
        <div style="height: 10px;"></div>
        <div style="clear: both;"></div>        
        
        <div class="O_group">
            <?php if (count($outcome_groups)) {
                        foreach($outcome_groups as $group) {                                                         
                            $params = array (
                                "group" => $group,
                                "id_account" => $id_account
                            );  
                            echo $this->card_categories($params);
                        } 
            } else { ?>
                        <div class="alert alert-danger text-center">
                          <strong>Ooops!</strong> A&uacute;n no tienes categorias aqu&iacute;.
                        </div>
            <?php } ?>            
        </div>
        
        <div class="I_group hidden">
            <ul class="list-group">
            <?php if (count($income_groups)) {
                        foreach($income_groups as $group) {                                                        
                            $params = array (
                                "group" => $group
                            );  
                            echo $this->card_categories($params);
                        }               
            } else { ?>
                    <div class="alert alert-danger text-center">
                      <strong>Ooops!</strong> A&uacute;n no tienes categorias aqu&iacute;.
                    </div>
            <?php } ?>            
            </ul>
        </div> 
        
        <div class="card" style="border: 1px solid #ccc; border-radius:6px; margin-bottom:5px; padding:5px; height:45px;">
                     <?php if ($limits_account['categories']) { ?>     
                     <button type="button" class="btn btn-success  btn-block add-category"><i class="fa fa-plus-circle" aria-hidden="true"></i> Agregar categoria</button>
                     <?php } else { ?>
                     <button type="button" class="btn btn-success  btn-block view-premium"><i class="fa fa-plus-circle" aria-hidden="true"></i> Agregar categoria</button>
                     <?php } ?>
        </div>
        
    
<script>
    var email_user = '<?php echo $this->user_data->email; ?>';
        Android.registerDevice(email_user);
</script>                                   

<?php
    require("layouts/footer.php");
?>