<?php
if (!defined('simplemvc_exec')) {
    die('No tiene acceso directo a este recurso');
}

extract($view_data);

$this->Header($view_data);
$this->addScripts('dashboard.js');
    
?>
    <?php echo $this->whoami(); ?>
    <h4>Opppss!!</h4>
    
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row">
            Esto no deberia haber ocurrido, no deberias estar viendo esto, pero est&aacute;s aqu&iacute;, as&iacute; que trataremos de buscar una soluci&oacute;n. No est&aacute;s sol@, te acompa&nacute;o:
        </div>
        <div class="row">
            Esto puede haber ocurrido por:
            <ul>
                <ul>Has tratado de hacer algo que no debias.</ul>
                <ul>Algo se ha roto.</ul>
                <ul>Algo sali&oacute; mal.</ul>
                <ul>Se nos ha colado un duende en la aplicaci&oacute;n...</ul>                
            </ul>
        </div>
        <div class="row">
            No te preocupes, en unos segundos te llevamos al inicio. O si prefieres ir ya, dale <a href="index.php">aqu�</a>
        </div>
        
    </div>
<script>
     $(document).ready(function(){
          setTimeout(function() {
           window.location.href = "index.php?page=dashboard";
          }, 10000);
        });
</script>    
      
<?php
    require("layouts/footer.php");
?>