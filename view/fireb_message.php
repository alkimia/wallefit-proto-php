<?php
if (!defined('simplemvc_exec')) {
    die('No tiene acceso directo a este recurso');
}

extract($view_data);

$this->Header($view_data);
$this->addScripts('fireb_messages.js');


if ($id_message_type = 1) {    
?>
    <h1 align="center">¡No soy adivino!</h1>
    
    <div style="clear: both;"></div>
    <div style="height: 25px;"></div>
    <div style="clear: both;"></div>
    
    
    <p align="center"><strong>No hay datos de de hoy!</strong> ¿Has realizado algun gasto o has tenido algun ingreso?<br /><strong>Registralo ahora</strong></p>
    
    <div style="clear: both;"></div>
    <div style="height: 25px;"></div>
    <div style="clear: both;"></div>
    
    <button type="button" class="btn btn-success btn-lg btn-block new-credit">Anotar ingreso</button>
    <button type="button" class="btn btn-warning btn-lg btn-block new-debit">Anotar gasto</button>
    
    <div style="clear: both;"></div>
    <div style="height: 100px;"></div>
    <div style="clear: both;"></div>    
    
    <p align="center">Puedes configurar la recepción de mensajes.</p>
    
    <button type="button" class="btn btn-info btn-block disable-notif">No recibir más notificaciones</button>          
    <button type="button" class="btn btn-info btn-block config-notif">Configurar notificaciones</button>
    <button type="button" class="btn btn-info btn-block all-notif">Ver todas notificaciones</button>
<?php
}
?>    
<?php
    require("layouts/footer.php");
?>