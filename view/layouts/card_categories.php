<?php
    extract($params);
    
    if ($group['type'] == 'I') {
        $group_income = $group['id'];    
    } else {
        $group_outcome = $group['id'];
    }
    
    $link_detail ='index.php?page=timeline&type=' . $group['type'] . '&group_outcome=' . $group_outcome . '&group_income=' . $group_income . '&id_account=' . $id_account;
        
?>
    <div class="card" style="border: 1px solid #ccc; border-radius:6px; background-color:#fff; margin-bottom:5px; padding:5px; height:55px;">
            <div class="pull-right">                                        
                <button type="button" class="btn btn-primary btn-sm edit-category" data-id="<?php echo $group['id']; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                <?php if ($group['system'] == 1) { ?>                                        
                    <button type="button" class="btn btn-warning btn-sm delete-category-nope"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                <?php } else { ?>
                    <button type="button" class="btn btn-warning btn-sm delete-category"  data-id="<?php echo $group['id']; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                <?php } ?>
            </div>
            
            <div class="col-md-10 col-sm-10 col-xs-8">
                <a style="color:#000;" href="<?php echo $link_detail; ?>"><i class="fa fa-info-circle fa-lg" style="color:#46b8da;" aria-hidden="true"></i> <span class="card-title"><?php echo $group['name']; ?></span><br />
                <?php echo number_format($group['total'],2,',','.') ?> &euro;</a>
            </div>
    </div>