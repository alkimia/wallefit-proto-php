<?php
    extract($params);
?>
<div class="row">
<div class="card" style="background-color:#fff; border: 1px solid #ccc; border-radius:6px; margin-bottom:5px; padding:5px; height:120px;">
        <div class="col-md-3 col-sm-3">
            <span class="card-title"><b><?php echo $username ?></b></span>
        </div>
        <div class="col-md-9 col-sm-9" style="min-height: 50px;">
            <?php if ($description == '') { ?>
                Me encanta Wallefit!
            <?php } else {
                echo $description;
            } ?>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
        <?php
        $params_check_permission = array (
            "id_account" => $this->urldata['id_account'],
            "id_user" => $id,
            "can_read" => true
        );        
        
        if (!wallet::checkUserAccountPermissions($params_check_permission)) { ?>                                        
            <select name="permissions[<?php echo $id; ?>]" class="form-control">
                <option value="">- No compartir</option>
<?php /*                
                <option value="r" <?php if ($can_r) { echo ' selected'; } ?>>Solo puede ver</option>
*/ ?>                
                <option value="w" <?php if ($can_w) { echo ' selected'; } ?>>Puede ver y modificar</option>
            </select>
        <?php } else { ?>
            Cuenta ya compartida
        <?php } ?>                     

        </div>                
</div>
</div>