<?php
    extract($params);
?>
<div class="card" style="background-color:#fff; border: 1px solid #ccc; border-radius:6px; margin-bottom:5px; padding:5px; height:120px;">
        <div class="pull-right">                                        
            <button type="button" class="btn btn-warning btn-sm sharing-off" data-id="<?php echo $id_permission; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> No compartir</button>
        </div>

        <div class="col-md-3 col-sm-3">
            <span class="card-title"><b><?php echo $username ?></b></span>
        </div>
        <div class="col-md-9 col-sm-9" style="min-height: 50px;">
            <?php if ($description == '') { ?>
                Me encanta Wallefit!
            <?php } else {
                echo $description;
            } ?>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">                                        
                    <select name="permissions[<?php echo $to_user; ?>]" class="form-control">
                    <?php /*
                        <option value="r" <?php if ($can_r) { echo ' selected'; } ?>>Solo puede ver</option>
                    */ ?>                        
                        <option value="w" <?php if ($can_w) { echo ' selected'; } ?>>Puede ver y modificar</option>
                    </select>

        </div>                
</div>