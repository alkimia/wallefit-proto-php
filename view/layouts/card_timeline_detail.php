<?php
    extract($params);
?>
<div class="card" style="background-color:#fff; border: 1px solid #ccc; border-radius:6px; margin-bottom:5px; padding:5px; height:120px;">
        <?php if ($this->urldata['type'] == '') { ?>
        <div class="pull-right">                                        
            <span class="import_positive"><?php echo number_format($balance,2,',','.'); ?> &euro;</span>
        </div>
        <?php } ?>

        <div class="col-md-3 col-sm-3">
            <span class="card-title"><small><?php echo $date; ?></small></span>
        </div>
        <div class="col-md-9 col-sm-9">
            <?php echo $concept; ?> (<?php echo $category_description; ?>)<br /><?php 
            if ($credit > 0) {
                echo '<span class="import_positive">+ '.number_format($credit,2,',','.').' &euro;</span>';
            } elseif ($debit > 0) {
                echo '<span class="import_negative">- '.number_format($debit,2,',','.').' &euro;</span>';
            } ?>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">                                        
                    <button type="button" class="btn btn-primary btn-sm edit-note" data-id="<?php echo $id; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                    <button type="button" class="btn btn-danger btn-sm delete-note" data-id="<?php echo $id; ?>"><i class="fa fa-trash" aria-hidden="true"></i></button>
        </div>                
</div>