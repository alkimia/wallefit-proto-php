    <?php if (isset($this->success_message) && $this->success_message != '') { ?>  
    <div class="alert alert-success">
      <?php echo $this->success_message; ?>
    </div>       
    <?php } ?>
    
    <?php if (isset($this->info_message) && $this->info_message != '') { ?>  
    <div class="alert alert-info">
      <?php echo $this->info_message; ?>
    </div>
    <?php } ?>
    
    <?php if (isset($this->warning_message) && $this->warning_message != '') { ?>
    <div class="alert alert-warning">
      <?php echo $this->warning_message; ?>
    </div>        
    <?php } ?>
    
    <?php if (isset($this->danger_message) && $this->danger_message != '') { ?>
    <div class="alert alert-danger">
      <?php echo $this->danger_message; ?>
    </div>
    <?php } ?>