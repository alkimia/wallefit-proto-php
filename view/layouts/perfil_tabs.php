<?php
    extract($params);
    
    if ($this->urldata['tab'] == '') {
        $active_tab1 = 'active';
        
    } elseif ($this->urldata['tab'] == 'perfil-cuentas' || $this->urldata['tab'] == 'perfil-compartir') {
        $active_tab3 = 'active';
        
    } elseif ($this->urldata['tab'] == 'perfil-notificaciones') {
        $active_tab2 = 'active';
        
    } elseif ($this->urldata['tab'] == 'perfil-invitaciones') {
        $active_tab4 = 'active';
    }

?>
        <div class="btn-group btn-group-justified">
          <div class="btn-group">
            <button type="button" class="btn btn-primary btn-my-settings <?php echo $active_tab1; ?>">Perfil</button>
          </div>
          <div class="btn-group">
            <button type="button" class="btn btn-primary btn-my-notifications <?php echo $active_tab2; ?>">Avisos</button>
          </div>  
          <div class="btn-group">
            <button type="button" class="btn btn-primary btn-my-accounts <?php echo $active_tab3; ?>">Cuentas</button>
          </div>
          <div class="btn-group">
            <button type="button" class="btn btn-primary btn-invited-accounts <?php echo $active_tab4; ?>">Invitaciones</button>
          </div>
        </div> 