<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.php?page=dashboard"> <i class="fa fa-home" aria-hidden="true"></i> Wallefit</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav" style="padding-left: 10px;">
        <li><a style="font-size:18px;" href="javascript:window.history.back();"><i class="fa fa-undo header-back" aria-hidden="true"></i> Volver</a></li>
        <li><a style="font-size:18px;" href="index.php?page=note&type=I&id_account=<?php echo $id_account; ?>"><i class="fa fa-plus header-income" aria-hidden="true"></i> Ingreso</a></li>
        <li><a style="font-size:18px;" href="index.php?page=note&type=O&id_account=<?php echo $id_account; ?>"><i class="fa fa-minus" aria-hidden="true"></i> Gasto</a></li>
        <li><a style="font-size:18px;" href="index.php?page=timeline&id_account=<?php echo $id_account; ?>"><i class="fa fa-list-ol" aria-hidden="true"></i> Extracto</a></li>
        <li><a style="font-size:18px;" href="#" class="link_to_perfil"><i class="fa fa-user-o" aria-hidden="true"></i> Perfil</a></li>

      </ul>
<?php /*
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Acciones <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Nuevo gasto</a></li>
            <li><a href="#">Nuevo ingreso</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Perfil</a></li>
          </ul>
        </li>
      </ul>
*/ ?>      
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<div class="panel panel-default">
  <div class="panel-body">
    <div class="col-sm-10 col-md-10 col-xs-10">
        <i class="fa fa-user-o" aria-hidden="true"></i> Conectado como <a href="index.php?page=perfil"><?php echo $this->user_data->username; ?></a>
    </div>
    <div class="col-sm-2 col-md-2 col-xs-2" align="right">
        <button type="button" class="btn btn-warning btn-sm">
            <a href="index.php?page=login&action=logout"><i style="color:#fff;" class="fa fa-power-off" aria-hidden="true"></i></a>
        </button>
    </div>
  </div>
</div>