<?php
if (!defined('simplemvc_exec')) {
    die('No tiene acceso directo a este recurso');
}

extract($view_data);

$this->Header($view_data);
$this->addScripts('login.js');
    
?>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '720962631411366'); // Insert your pixel ID here.
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=720962631411366&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->

    <div class="contenedor-login" style="text-align: center; margin-top:50px;">
    
   
        
        <form class="form-signin" method="post" action="">
            <h3>Wallefit</h3>
            <h4>Descubre agujeros, controla tu dinero</h4>
            
            <div style="clear: both;"></div>
            <div style="height: 40px;"></div>
            <div style="clear: both;"></div>
                        
            <p>Si dispones de usuario y clave, entra ahora</p>
            <label for="name" name="name" class="sr-only">Nombre usuario</label>
            <input type="text" id="name" name="name" class="form-control username-login" placeholder="nombre" maxlength="15" required>
            <label for="pass" name="password" class="sr-only">Password</label>
            <input type="password" id="pass" name="password" class="form-control" placeholder="password" required>
            
            <p style="padding-top:10px;"><a href="index.php?page=recovery_pass">Opps! he olvidado la clave</a></p>
            
            <div style="clear: both;"></div>
            <div style="height: 20px;"></div>
            <div style="clear: both;"></div>
            
            <?php if (isset($this->danger_message) && $this->danger_message != '') { ?>
                <div class="alert alert-danger">
                   <?php echo $this->danger_message; ?>
                </div>
                
                <div style="clear: both;"></div>
                <div style="height: 10px;"></div>
                <div style="clear: both;"></div>
            
            <?php } ?>             
            
            <button class="btn btn-success btn-block" type="submit">Entrar</button>
            
            <p style="padding-top: 35px; padding-bottom:10px;">Si aun no dispones de usuario y clave, registrate ahora</p>
            
            <button class="btn btn-primary btn-block btn-registro" type="button">Registro</button>
                <div style="clear: both;"></div>
                <div style="height: 100px;"></div>
                <div style="clear: both;"></div>            
            <input type="hidden" name="action" value="login">
            <input type="hidden" name="page" value="login">
        </form>
        
        <span id="siteseal"><script async type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=wT0yKXwwTbQ5fuHy7974pH6Q0SQu3K6Ob82PbMgqPaTLkMXYYDnqLXVmWsfU"></script></span>
        
        
    </div>
      
<?php
    require("layouts/footer.php");
?>