<?php
if (!defined('simplemvc_exec')) {
    die('No tiene acceso directo a este recurso');
}

extract($view_data);

$this->Header($view_data);
    
?>
<div class="container-fluid">

  <form class="form-signin" method="post" action="">
    <h3>Inserte sticky</h3>
    <label for="descrption" name="descrption" class="sr-only">Descripción</label>
    <textarea name="description"><?php echo $data['description']; ?></textarea>
    
    <label for="status" name="status" class="sr-only">Estado</label>
        <select name="id_status" class="form-control">
            <option value="">- Seleccione</option>
            <option value="1" <?php if ($data['id_status'] == 1) { echo ' selected'; } ?>>Abierto</option>
            <option value="2" <?php if ($data['id_status'] == 2) { echo ' selected'; } ?>>En Proceso</option>
            <option value="3" <?php if ($data['id_status'] == 3) { echo ' selected'; } ?>>Cerrado</option>
            
        </select>        
    
    <input type="hidden" name="id" value="<?php if (isset($data['id'])) { echo $data['id']; } ?>">
    <button class="btn btn-default" type="submit">Guardar</button>
    <button class="btn btn-default cancel-form-sticky" type="submit">Cancelar</button>
  </form>

</div> <!-- /container -->
<?php
    require("layouts/footer.php");
?>