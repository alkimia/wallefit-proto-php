<?php
if (!defined('simplemvc_exec')) {
    die('No tiene acceso directo a este recurso');
}

extract($view_data);

$this->Header($view_data);
$this->addScripts('news.js');
        
?>
<div id="fb-root"></div>
    <?php echo $this->whoami(); ?>
          
    <div class="col-md-12 col-xs-12 col-sm-12">
        <h4>Recibe notificaciones tipo whatsapp</h4>
        <p>Ahora puedes activar y configurar diferentes tipos de alertas que recibirás en tu móvil. Esta opción solo esta disponible para instalaciones de nuestra app.</p>        
        <button type="button" class="btn btn-success  btn-block btn-go-notifications"><i class="fa fa-info-circle" aria-hidden="true"></i> Activa las notificaciones ahora</button>
        
        <h4>Buscador mejorado</h4>
        <p>Dispones de una nueva opción de buscar gastos o ingresos por el nombre o el texto que lo guardaste. Ahora es más rápido y fácil consultar cualquier apunte.</p>
        <button type="button" class="btn btn-success  btn-block btn-go-search-engine"><i class="fa fa-info-circle" aria-hidden="true"></i> Prueba el nuevo buscador</button>

        <h4>Recibe ofertas y vales descuento de tu ciudad</h4>
        <p>Recibe notificaciones sobre ofertas y vales de descuento en mi ciudad que tengan relación con mis hábitos de compra.</p>
        <button type="button" class="btn btn-success  btn-block btn-go-notifications"><i class="fa fa-info-circle" aria-hidden="true"></i> Activa la notificación de ofertas</button>
<?php /*        
        <h4>Cuentas modo presupuesto</h4>
        <p>Estas cuentas funcionan de forma diferente: solo admite gastos que se van anotando y se acumula en tu total. Ideal para hacer presupuestos o conocer cuanto te esta
        suponiendo en gasto algun proyecto.</p>
        <button type="button" class="btn btn-success  btn-block btn-go-accounts"><i class="fa fa-info-circle" aria-hidden="true"></i> Crea una cuenta tipo presupuesto</button>
*/ ?>        
    </div>

    

<?php
    require("layouts/footer.php");
?>