<?php
if (!defined('simplemvc_exec')) {
    die('No tiene acceso directo a este recurso');
}

extract($view_data);

if (isset($data_movement['success_message'])) {
    $data_movement = $data_movement['success_message'];
}

$this->Header($view_data);
$this->addScripts('note.js');
    
?>
    <?php echo $this->whoami(); ?>
    
        <h4>Nuevo apunte</h4>
        
        <?php echo $this->messagesBox(); ?>
        
        <?php
            $params = array (
                "add_buttons" => false
            );  
            echo $this->accountFilter($params); 
        ?>        
        
        <form name="form_add_movement" action="" method="post">
                
        <div class="form-group">
            <div class="row">                
                <div class="col-sm-12 col-xs-12">
                    <label for="id_tipo">Fecha</label>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3 col-md-3 col-xs-12">
                    <select name="dd" id="dd" class="form-control">
                <?php 
                if (isset($data_movement['datetime_selected'])) {
                    $parts_date = explode ('-', $data_movement['datetime_selected']);
                    $day = $parts_date[2]; 
                       
                } else {
                    $day = date("d");
                }
                
                for($i=1; $i<32; ++$i) {
                    if ($i < 10) {
                        $idx = '0' . $i;
                    } else {
                        $idx = $i;
                    }
                    
                    ?>
                        <option value="<?php echo $idx; ?>" <?php if ($day == $idx) { echo ' selected'; } ?>><?php echo $idx; ?></option>
                <?php } ?>
                    </select>
                </div>
                <div class="col-sm-6 col-md-6 col-xs-12">
                    <select name="mm" id="mm" class="form-control">
                    <?php
                    $meses = array (
                        "01" => "Enero",
                        "02" => "Febrero",
                        "03" => "Marzo",
                        "04" => "Abril",
                        "05" => "Mayo",
                        "06" => "Junio",
                        "07" => "Julio",
                        "08" => "Agosto",
                        "09" => "Septiembre",
                        "10" => "Octubre",
                        "11" => "Noviembre",
                        "12" => "Diciembre"
                    );
                    
                    if (isset($data_movement['datetime_selected'])) {
                        $parts_date = explode ('-', $data_movement['datetime_selected']);
                        $month = $parts_date[1]; 
                           
                    } else {
                        $month = date("m");
                    }
                    
                    for($i = 1; $i < 13; ++$i) {
                        if ($i < 10) {
                            $idx = '0' . $i;
                        } else {
                            $idx = $i;
                        }                        
                        ?>
                        <option value="<?php echo $idx; ?>" <?php if ($month == $idx) { echo ' selected'; } ?>><?php echo $meses[$idx]; ?></option>
                <?php } ?>                    
                    </select>
                </div>
                <div class="col-sm-3 col-md-3 col-xs-12">
                    <select name="yy" id="yy" class="form-control">
                    <?php
                    
                    $year_init = date("Y") - 2;
                    $year_end = date("Y") + 1;
                    
                    if (isset($data_movement['datetime_selected'])) {
                        $parts_date = explode ('-', $data_movement['datetime_selected']);
                        $year = $parts_date[0]; 
                           
                    } else {
                        $year = date("Y");
                    }
                    
                    for($i = $year_init ; $i < $year_end; ++$i) { ?>
                        <option value="<?php echo $i; ?>" <?php if ($year == $i) { echo ' selected'; } ?>><?php echo $i; ?></option>
                    <?php } ?>                    
                    </select>
                </div>
            </div>        
            
            <div class="row">
                <div class="col-sm-12 col-xs-12">
                    <label for="id_tipo">Tipo</label>
                    <?php if ($data_movement['credit'] > 0) {
                            $type_movement = 'I';
                        } elseif ($data_movement['debit'] > 0) {
                            $type_movement = 'O';
                        } else {
                            $type_movement = $this->urldata['type'];
                        }
                    ?>                        
                    <select name="type" id="type" class="form-control">
                        <option value="I" <?php if ($type_movement == 'I') { echo ' selected'; } ?>>Ingreso</option>
                        <option value="O" <?php if ($type_movement == 'O') { echo ' selected'; } ?>>Gasto</option>
                    </select>
                </div>
            </div>
        </div>
        
        <div class="form-group">
            <div class="O_group <?php echo $o_group; ?>">
              <label for="usr">Categoria gastos:</label>
                <select name="group_outcome" class="form-control selector-outcome">
                <option value="">- Selecciona</option>
                    <?php foreach($outcome_groups as $group) { ?>
                      <option value="<?php echo $group['id']; ?>" <?php if ($data_movement['id_outcome_group'] == $group['id']) { echo ' selected'; } ?>><?php echo $group['descripcion']; ?></option>
                    <?php } ?>
                        <option value="-1">Agregar categoria</option>        
                </select>
          
            </div>
        <?php 
            if ($this->urldata['type'] == 'I') {
                $i_group = '';
            } else {
                if ($data_movement['credit'] > 0) {
                    $i_group = '';
                } elseif ($data_movement['debit'] > 0) {
                    $i_group = ' hidden';
                }
            } 
        ?>              
            <div class="I_group  <?php echo $i_group; ?>">
              <label for="usr">Categoria ingresos:</label>
                <select name="group_income" class="form-control selector-income">
                    <option value="">- Selecciona</option>
                    <?php foreach($income_groups as $group) { ?>
                      <option value="<?php echo $group['id']; ?>" <?php if ($data_movement['id_income_group'] == $group['id']) { echo ' selected'; } ?>><?php echo $group['descripcion']; ?></option>
                    <?php } ?>
                        <option value="-1">Agregar categoria</option>            
                </select>
           
            </div>            
       </div>
       
               
        
        <div class="form-group">
          <label for="usr">Notas:</label>
          <textarea class="form-control" id="comments" name="comments"><?php echo $data_movement['concept']; ?></textarea>          
        </div>
        
        <div class="form-group">
            <div class="row">
                <div class="col-sm-12 col-xs-12 col-md-12">
                    <label>Importe</label>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-10 col-xs-10 col-md-10">
                    <input type="text" class="form-control right-text" id="amount" name="amount" value="<?php 
                        if ($data_movement['credit'] != 0) { echo $data_movement['credit']; } 
                        if ($data_movement['debit'] != 0) { echo $data_movement['debit']; }  
                        ?>" style="font-size:24px; text-align: right;">
                </div>
                <div class="col-sm-2 col-xs-2 col-md-2">
                    <button type="button" class="btn btn-default btn-lg btn-block btn-retro"><</button>
                </div>
            </div>
        </div>
        <?php 
            if ($this->urldata['type'] == 'O') {
                $o_group = '';
            } else {
                if ($data_movement['credit'] > 0) {
                    $o_group = ' hidden';
                } elseif ($data_movement['debit'] > 0) {
                    $o_group = '';
                }
            } 
        ?>  
        
        
        
        <div class="card" style="border:1px solid #ccc; border-radius:6px; padding:10px;">
            <div class="row">
                <div class="col-sm-4 col-xs-4">
                    <button type="button" class="btn btn-default btn-lg btn-block btn-calculator" data-calculator="7">7</button>    
                </div>
                <div class="col-sm-4 col-xs-4">
                    <button type="button" class="btn btn-default btn-lg btn-block btn-calculator" data-calculator="8">8</button>
                </div>
                <div class="col-sm-4 col-xs-4">
                    <button type="button" class="btn btn-default btn-lg btn-block btn-calculator" data-calculator="9">9</button>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 col-xs-4">
                    <button type="button" class="btn btn-default btn-lg btn-block btn-calculator" data-calculator="4">4</button>    
                </div>
                <div class="col-sm-4 col-xs-4">
                    <button type="button" class="btn btn-default btn-lg btn-block btn-calculator" data-calculator="5">5</button>
                </div>
                <div class="col-sm-4 col-xs-4">
                    <button type="button" class="btn btn-default btn-lg btn-block btn-calculator" data-calculator="6">6</button>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 col-xs-4">
                    <button type="button" class="btn btn-default btn-lg btn-block btn-calculator" data-calculator="1">1</button>    
                </div>
                <div class="col-sm-4 col-xs-4">
                    <button type="button" class="btn btn-default btn-lg btn-block btn-calculator" data-calculator="2">2</button>
                </div>
                <div class="col-sm-4 col-xs-4">
                    <button type="button" class="btn btn-default btn-lg btn-block btn-calculator"  data-calculator="3">3</button>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 col-xs-4">
                    <button type="button" class="btn btn-default btn-lg btn-block btn-calculator"  data-calculator=".">.</button>    
                </div>
                <div class="col-sm-4 col-xs-4">
                    <button type="button" class="btn btn-default btn-lg btn-block btn-calculator"  data-calculator="0">0</button>
                </div>
                <div class="col-sm-4 col-xs-4">
                    <button type="button" class="btn btn-default btn-lg btn-block btn-calculator-clear">Borra</button>
                </div>
            </div>                                     
        </div>
        

            <input type="hidden" name="action" value="add_movement">
<?php
                                if ($data_movement['id_account'] != '') {
                                    $id_account = $data_movement['id_account'];
                                } ?>            
            <input type="hidden" name="id_account" value="<?php echo $id_account; ?>">
            <input type="hidden" name="id_movement" value="<?php echo $data_movement['id']; ?>">
       </form> 
       
    <div class="add_category hidden"> 
        <form name="form_add_category" action="" method="POST">
            <div class="col-sm-12 col-xs-12">
                     <div class="form-group">
                        <div class="row">  
                            <label>Nombre nueva categoria:</label>              
                            <div class="col-sm-12 col-xs-12">
                                <input type="text" class="form-control" id="category_name" name="category_name">
                                <input type="hidden" name="action" value="add_category">                             
                                <input type="hidden" name="id_account" value="<?php echo $id_account; ?>">
                                <input type="hidden" name="type" id="type" value="<?php echo $this->urldata['type']; ?>">
                                
                            </div>

                                <?php if ($limits_account['categories']) { ?>  
                                <button type="button" class="btn btn-primary btn-sm btn-send-category">Agregar</button>
                                <?php } else { ?>
                                <button type="button" class="btn btn-primary btn-sm view-premium">Agregar</button>
                                <?php } ?>
                                </div>

                        </div>
                    </div>
     
            </div>
        </form>
    </div>       
       

        
    <div class="col-sm-12 col-xs-12">
        <div class="row">
            <button type="button" class="btn btn-success btn-lg btn-block btn-save-note">Guardar</button>
        </div>
    </div>    

<?php
    require("layouts/footer.php");
?>