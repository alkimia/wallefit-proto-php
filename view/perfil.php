<?php
if (!defined('simplemvc_exec')) {
    die('No tiene acceso directo a este recurso');
}

extract($view_data);

if (isset($data_movement['success_message'])) {
    $data_movement = $data_movement['success_message'];
}

$this->Header($view_data);
$this->addScripts('perfil.js');
    
?>
    <?php echo $this->whoami(); ?>
    
        <h4>Perfil usuario y cuentas</h4>
        
        <?php echo $this->messagesBox(); ?>
        
        <?php 
        $params_enable_tabs = array (
        );
        echo $this->perfilTabs($params_enable_tabs); 
        ?>    
        
        <div id="datos_usuario">
            <div class="col-sm-12 col-xs-12 col-md-12" style="padding-top: 20px;">
                <div class="row" style="padding-left: 10px; padding-right:10px;">        
                    Los datos que indiques aquí son los únicos que verán otros usuarios. Esto ayuda a localizarte fácilmente si decides
                    compartir tus cuentas con otros usuarios.
                </div>
                
                <form method="post" action="index.php?page=perfil" name="form_data_profile">
            
            <div class="form-group">                    
                <div class="row">
                        <label>Descripción:</label>
                        <textarea name="description" class="form-control"><?php echo $data_profile['description']; ?></textarea>    
                </div>
            </div>
            
            <div class="form-group">                    
                <div class="row">
                        Los datos a continuación son obligatorios para acceder a todas las caracteristicas y funciones de wallefit, además nos ayudará a prestarte mejores servicios y ofertas.    
                </div>
            </div> 
            
            <div class="form-group">
                <div class="row">
                    <label>E-mail</label>
                </div>                    
                <div class="row">
                    <input type="email" name="email" required="true" class="form-control" value="<?php echo $data_profile['email']; ?>">
                </div>
            </div>                       
            
            <div class="form-group">
                <div class="row">
                    <label>Código Postal</label>
                </div>                    
                <div class="row">
                    <input type="text" name="cp" required="true" class="form-control" value="<?php echo $data_profile['postalcode']; ?>">
                </div>
            </div> 
            
            <div class="form-group">
                <div class="row">
                    <label>Nombre</label>
                </div>                    
                <div class="row">
                    <input type="text" name="name" required="true" class="form-control" value="<?php echo $data_profile['name']; ?>">
                </div>
            </div>                                             
                    
            <div class="form-group">
                <div class="row">
                    <label>Apellidos</label>
                </div>                    
                <div class="row">
                    <input type="text" name="surname" required="true" class="form-control" value="<?php echo $data_profile['surname']; ?>">
                </div>
            </div>                                             


            <div class="form-group">
                <div class="row">
                    <label>Fecha nacimiento:</label>
                </div>                    
                <div class="row">
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <select name="dd" id="dd" class="form-control">
                            <option value="">-</option>
                    <?php 
                    if (isset($data_profile['birthdate_dd'])) {
                        $day = $data_profile['birthdate_dd']; 
                           
                    }
                    
                    for($i=1; $i<32; ++$i) {
                        if ($i < 10) {
                            $idx = '0' . $i;
                        } else {
                            $idx = $i;
                        }
                        
                        ?>
                            <option value="<?php echo $idx; ?>" <?php if ($day == $idx) { echo ' selected'; } ?>><?php echo $idx; ?></option>
                    <?php } ?>
                        </select>
                    </div>
                    <div class="col-sm-6 col-md-6 col-xs-12">
                        <select name="mm" id="mm" class="form-control">
                        <option value="">-</option>
                        <?php
                        $meses = array (
                            "01" => "Enero",
                            "02" => "Febrero",
                            "03" => "Marzo",
                            "04" => "Abril",
                            "05" => "Mayo",
                            "06" => "Junio",
                            "07" => "Julio",
                            "08" => "Agosto",
                            "09" => "Septiembre",
                            "10" => "Octubre",
                            "11" => "Noviembre",
                            "12" => "Diciembre"
                        );
                        
                        if (isset($data_profile['birthdate_mm'])) {
                            $month = $data_profile['birthdate_mm']; 
                               
                        }
                        
                        for($i = 1; $i < 13; ++$i) {
                            if ($i < 10) {
                                $idx = '0' . $i;
                            } else {
                                $idx = $i;
                            }                        
                            ?>
                            <option value="<?php echo $idx; ?>" <?php if ($month == $idx) { echo ' selected'; } ?>><?php echo $meses[$idx]; ?></option>
                    <?php } ?>                    
                        </select>
                    </div>
                    <div class="col-sm-3 col-md-3 col-xs-12">
                        <select name="yy" id="yy" class="form-control">
                            <option value="">-</option>
                        <?php
                        
                        $year_init = date("Y") - 90;
                        $year_end = date("Y") - 15;
                        
                        if (isset($data_profile['birthdate_yy'])) {
                            $year = $data_profile['birthdate_yy']; 
                               
                        } 
                        
                        for($i = $year_init ; $i < $year_end; ++$i) { ?>
                            <option value="<?php echo $i; ?>" <?php if ($year == $i) { echo ' selected'; } ?>><?php echo $i; ?></option>
                        <?php } ?>                    
                        </select>
                    </div>
                </div>
            </div> 
            
            <div class="form-group">
                <div class="row">
                    <label>Clave acceso</label>
                </div>                    
                <div class="row">
                    <input type="password" name="pass" id="pass" class="form-control">
                </div>
                <div class="row">
                    <label>Repite clave</label>
                    <input type="password" name="repass" id="repass" class="form-control">
                </div>                
            </div>
            
            <div class="form-group">
                <div class="row">
                    Si no recuerdas tu clave te haremos la pregunta secreta. Solo tu puedes conocer la respuesta para reestablecer la clave de acceso.
                </div>
                <div class="row">
                    <label>Pregunta secreta</label>
                </div>                    
                <div class="row">
                    <input type="text" name="secret_question" id="secret_question" value="<?php echo $data_profile['secret_question']; ?>" class="form-control">
                </div>
                <div class="row">
                    <label>Respuesta a la pregunta secreta</label>
                    <input type="text" name="secret_answer" id="secret_answer" value="<?php echo $data_profile['secret_answer']; ?>" class="form-control">
                </div>                
            </div>                            
            
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="form-group"> 
                    <div class="row">                
                        <div class="row text-center">                
                            <button type="button" class="btn btn-success btn-sm btn-lg btn-block btn-save-profile">Actualizar</button>
                        </div>
                    </div>
                </div>
            </div>
                    
            
                    
            <input type="hidden" name="action" value="update_profile">
            <input type="hidden" name="page" value="perfil">
            <input type="hidden" name="premsg" value="<?php echo $this->urldata['premsg']; ?>">
                
            </form>
                                
        </div>            
            
    </div>
        

<?php
    require("layouts/footer.php");
?>