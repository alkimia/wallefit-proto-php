<?php
if (!defined('simplemvc_exec')) {
    die('No tiene acceso directo a este recurso');
}

extract($view_data);

if (isset($data_movement['success_message'])) {
    $data_movement = $data_movement['success_message'];
}

$this->Header($view_data);
$this->addScripts('perfil.js');
    
?>
    <?php echo $this->whoami(); ?>
    
        <h4>Perfil usuario y cuentas</h4>
                
        <?php echo $this->messagesBox(); ?>
        
        <?php 
        $params_enable_tabs = array (
        );
        echo $this->perfilTabs($params_enable_tabs); 
        ?> 

        <h4>Compartir cuenta: <?php echo $account_name; ?></h4>
        
        <?php
            if ($res_users_sharing) { ?>
         <div class="col-sm-12 col-xs-12 col-md-12">                
            <p>Compartes esta cuenta con estos usuarios:</p>
        
            <form name="account_share" action="index.php?page=perfil&tab=perfil-compartir&id_account=<?php echo $this->urldata['id_account']; ?>" method="post">
                <?php 
                foreach($res_users_sharing as $user) {
                    
                    $users_sharing[] = $user['to_user'];                    
                    echo $this->card_users_sharing($user);                
                } 
                
                ?>
              
                <button class="btn btn-success" type="submit">Guardar opciones</button>
                <input type="hidden" name="action" id="action" value="update_share">
                <input type="hidden" name="id_account" id="id_account" value="<?php echo $this->urldata['id_account']; ?>">
                
            </form>
         </div>
         <?php } else { ?>
         <div class="col-sm-12 col-xs-12 col-md-12">                
            <div class="alert alert-info">
              <strong>Aviso!</strong> No est&aacute;s compartiendo ninguna cuenta con usuarios
            </div>
        </div>         
         <?php } ?>        

        <div class="col-sm-12 col-xs-12 col-md-12">    
            <div class="row text-center">        
                Busca usuario de Wallefit con el que quieras compartir esta cuenta y establece los permisos.
            </div>
            
            <div class="row">    
                <form method="post" action="index.php?page=perfil&tab=perfil-compartir&id_account=<?php echo $this->urldata['id_account']; ?>">
                    <div class="col-sm-10 col-xs-12 col-md-10 text-left">
                        <input type="text" name="username" class="form-control">                                                    
                    </div>
                    <div class="col-sm-2 col-xs-12 col-md-2 text-center">
                        <button class="btn btn-info" type="submit">Buscar</button>
                    </div>
                <input type="hidden" name="action" value="search_user">
                </form>
            </div>
                
        </div>

        <div class="col-sm-12 col-xs-12 col-md-12" style="padding-top: 20px;;">
            <?php if ($res_users) { ?>
            
                                
                    <form name="account_share" action="index.php?page=perfil&tab=perfil-compartir&id_account=<?php echo $this->urldata['id_account']; ?>" method="post">
                     
                    <?php                    
                    foreach($res_users as $user) {
                        echo $this->card_users_search($user);
                    }            
                    ?>                    
                  
                    <button class="btn btn-success" type="submit">Aplicar permisos</button>
                    <input type="hidden" name="action" id="action" value="add_share">
                    <input type="hidden" name="id_account" value="<?php echo $this->urldata['id_account']; ?>">
                    
                    </form>                

            <?php } elseif ($_POST && (!$res_users)) { ?>
                <div class="alert alert-danger">
                  <strong>Sin resultados</strong> No se encontr&oacute; ningun usuario con ese nombre
                </div>
            <?php } ?>
        </div>            
                          
            
        
<?php
    require("layouts/footer.php");
?>