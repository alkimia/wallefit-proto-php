<?php
if (!defined('simplemvc_exec')) {
    die('No tiene acceso directo a este recurso');
}

extract($view_data);

if (isset($data_movement['success_message'])) {
    $data_movement = $data_movement['success_message'];
}

$this->Header($view_data);
$this->addScripts('perfil.js');
    
?>
    <?php echo $this->whoami(); ?>
    
        <h4>Perfil usuario y cuentas</h4>
        
        <?php echo $this->messagesBox(); ?>
        
        <?php 
        $params_enable_tabs = array (
        );
        echo $this->perfilTabs($params_enable_tabs); 
        ?>   
        
        <div id="datos_usuario">
            <div class="col-sm-12 col-xs-12 col-md-12" style="padding-top: 20px;">
                <div class="row" style="padding-left: 10px; padding-right:10px;">        
                    Aquí puedes gestionar tus cuentas en Wallefit. Crear nuevas, editarlas, borrarlas y decidir a qué otros usuarios de wallefit das acceso.
                </div>
                
                <?php if ($limits_account['accounts']) { ?>
                <button class="btn btn-info btn-add-account" type="button">Crear nueva</button>
                <?php } else { ?>
                <button class="btn btn-info view-premium" type="button">Crear nueva</button>
                <?php } ?>
                
                <form name="account_selection" action="index.php?page=perfil&tab=perfil-cuentas" method="post">
                    <table class="table table-hover">
                        <thead>
                          <tr>
                            <th>Nombre cuenta</th>
                            <th>Principal</th>
                            <th>Acciones</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php
                            foreach ($accounts as $item_account) {
                                
                                if ($item_account['principal']) {
                                    $checked = 'checked';
                                } else {
                                    $checked = '';
                                }
                                
                                ?>
                          <tr>
                            <td><?php echo $item_account['descripcion']; ?></td>
                            <td><input type="radio" name="account_selected" value="<?php echo $item_account['id']; ?>" class="form-control" style="width: 25px; height:25px;" <?php echo $checked; ?>></td>
                            <td>
                                <button class="btn btn-info btn-sm edit-account-name" data-id="<?php echo $item_account['id']; ?>" type="button"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                <?php if ($item_account['system']) { ?>
                                <button class="btn btn-danger btn-sm delete-account-nope" type="button"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                <?php } elseif ($item_account['principal']) { ?>
                                <button class="btn btn-danger btn-sm delete-account-nope-main" type="button"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                <?php } else { ?>
                                <button class="btn btn-danger btn-sm delete-account" data-id="<?php echo $item_account['id']; ?>" type="button"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                <?php } ?>
                                <button class="btn btn-primary btn-sm share-account" data-id="<?php echo $item_account['id']; ?>" type="button"><i class="fa fa-share-alt" aria-hidden="true"></i></button></td>
                          </tr>
                          <?php } ?>
                        </tbody>
                      </table>
                  
                    <button class="btn btn-success" type="submit">Guardar selección</button>
                    <input type="hidden" id="action_sel" name="action_sel" value="sel_account">
                    
                  </form>
                  
                  


                                
            </div>            
            
        </div>
        
        <form name="form_actions" action="index.php?page=perfil&tab=perfil-cuentas" method="post">
            <input type="hidden" id="action_fa" name="action_fa" value="add_account">
            <input type="hidden" id="name_account" name="name_account">
            <input type="hidden" id="account_id" name="account_id">         
        </form>
        

<?php
    require("layouts/footer.php");
?>