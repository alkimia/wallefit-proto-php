<?php
if (!defined('simplemvc_exec')) {
    die('No tiene acceso directo a este recurso');
}

extract($view_data);

if (isset($data_movement['success_message'])) {
    $data_movement = $data_movement['success_message'];
}

$this->Header($view_data);
$this->addScripts('perfil.js');
    
?>
    <?php echo $this->whoami(); ?>
    
        <h4>Perfil usuario y cuentas</h4>
                
        <?php echo $this->messagesBox(); ?>
        
        <?php 
        $params_enable_tabs = array (
        );
        echo $this->perfilTabs($params_enable_tabs); 
        ?> 

        <?php
            if ($res_count_invited) { ?>
         <div class="col-sm-12 col-xs-12 col-md-12">                
            <p>Estas como invitad@ en las siguientes cuentas:</p>


          <table class="table table-bordered" style="background-color:#fff;">
            <thead>
              <tr>
                <th>Cuenta</th>
                <th>Propietario</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody>
            <?php foreach ($res_count_invited as $account) { ?>                
                
              <tr>
                <td><?php echo $account['descripcion']; ?></td>
                <td><?php echo $account['owner']; ?></td>
                <td><button type="button" class="btn btn-warning btn-sm exit-account" data-id="<?php echo $account['id_permission']; ?>">Abandonar</button></td>
              </tr>
            <?php } ?>                
            </tbody>
          </table>

        
                
         </div>
         <?php } else { ?>
         <div class="col-sm-12 col-xs-12 col-md-12">
            <div class="alert alert-info">
              <strong>Info!</strong> No est&aacute;s invitado a ninguna cuenta ahora.
            </div>
        </div>         
         <?php } ?>        

        
<?php
    require("layouts/footer.php");
?>