<?php
if (!defined('simplemvc_exec')) {
    die('No tiene acceso directo a este recurso');
}

extract($view_data);

if (isset($data_movement['success_message'])) {
    $data_movement = $data_movement['success_message'];
}

$this->Header($view_data);
$this->addScripts('perfil.js');

//print_r($limits_account);
    
?>
    <?php echo $this->whoami(); ?>
    
        <div class="alert alert-danger text-center">
          Está opción está en desarrollo, no está disponible para el 100% de los dispositivos.<br />Puede dejar activada para hacer pruebas y dejar activada la notificación que más le interese.
        </div>     
    
        <h4>Perfil usuario y cuentas</h4>
                
        <?php echo $this->messagesBox(); ?>
        
        <?php 
        $params_enable_tabs = array (
        );
        echo $this->perfilTabs($params_enable_tabs); 
        ?> 

        <h4>Notificaciones</h4>
       
 
        <table width="100%">
            <tr>
                <td width="90%">  
                    <label>Desactivar todas las notificaciones</label>
                </td>
                <td align="center"> 
                    <input type="checkbox" class="form-control disable-all-notif" style="width: 20px;  height:20px;" id="disable_notif" <?php if (!notif::isNotificationsEnabled()) { echo ' checked'; } ?>>
                </td>
            </tr>
            <tr class="notif_option <?php echo $aditional_class; ?>">
                <td width="90%">  
                    <label>Recordatorio diario</label>
                    <p>Antes de que transcurra un dia sin que anotes datos, wallefit te notificara
                    con un recordatorio para que insertes los gastos o ingresos del dia.</p>
                </td>
                 <td align="center""> 
                    <input type="checkbox" class="form-control <?php echo $notification_class; ?>" style="width: 20px;  height:20px;" data-id="1" <?php if ($notification[1]) { echo ' checked'; } ?>> 
                </td>
            </tr>
            <tr class="notif_option <?php echo $aditional_class; ?>">
                <td width="90%">  
                    <label>Ofertas Locales</label>
                    <p>Notificarme sobre ofertas y vales de descuento en mi ciudad que tengan relación con mis hábitos de compra.</p>
                </td>
                 <td align="center"> 
                    <input type="checkbox" class="form-control <?php echo $notification_class; ?>" style="width: 20px;  height:20px;" data-id="6" <?php if ($notification[6]) { echo ' checked'; } ?>> 
                </td>
            </tr>             
            <tr class="notif_option <?php echo $aditional_class; ?>">
                <td width="90%">  
                    <label>Cuando los gastos sean superiores a los ingresos</label>
                    <p>Que no te vuelvan a decir que vives por encima de tus posibilidades. En el momento que tus gastos sean
                    iguales o superiores a tus ingresos, wallefit te notificara con una advertencia para que frenes o controles mejor los gastos.</p>
                </td>
                 <td align="center"> 
                    <input type="checkbox" class="form-control <?php echo $notification_class; ?>" style="width: 20px;  height:20px;" data-id="2" <?php if ($notification[2]) { echo ' checked'; } ?>>
                </td>
            </tr>
            <tr class="notif_option <?php echo $aditional_class; ?>">
                <td width="90%">  
                    <label>Limite de gasto semanal</label>
                    <p>Indica un tope de gasto semanal. Si lo superas, wallefit te enviara una notificacion de advertencia para que controles tus gastos.</p>
                </td>
                 <td align="center"> 
                    <input type="checkbox" class="form-control <?php echo $notification_class; ?>" style="width: 20px;  height:20px;" data-id="3" <?php if ($notification[3]) { echo ' checked'; } ?>>
                </td>
            </tr>
            <tr class="notif_option <?php echo $aditional_class; ?>">
                <td width="90%">  
                    <label>Limite de gasto por categoria</label>
                    <p>Indica un tope de gasto por categoria al mes. Si lo superas, wallefit te enviara una notificacion de advertencia para que controles tus gastos.</p>
                </td>
                 <td align="center"> 
                    <input type="checkbox" class="form-control <?php echo $notification_class; ?>" style="width: 20px;  height:20px;" data-id="4" <?php if ($notification[4]) { echo ' checked'; } ?>>
                </td>
            </tr> 
            <tr class="notif_option <?php echo $aditional_class; ?>">
                <td width="90%">  
                    <label>Recordatorio de pagos</label>
                    <p>Tienes un pago o gasto fijo al mes, como puede ser el alquiler, academias, colegios, etc?. Insertalo en la agenda y wallefit te notificará dias antes
                    de la fecha de pago.</p>
                </td>
                 <td align="center"> 
                    <input type="checkbox" class="form-control <?php echo $notification_class; ?>" style="width: 20px;  height:20px;" data-id="5" <?php if ($notification[5]) { echo ' checked'; } ?>> 
                </td>
            </tr>                       
            
        </table>
        
        <input type="hidden" name="id_owner" id="id_owner" value="<?php echo $this->id; ?>">                                
        
<?php
    require("layouts/footer.php");
?>