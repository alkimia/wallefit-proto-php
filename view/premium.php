<?php
if (!defined('simplemvc_exec')) {
    die('No tiene acceso directo a este recurso');
}

extract($view_data);

$this->Header($view_data);
$this->addScripts('premium.js');
        
?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v2.8&appId=503151023191903";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    <?php echo $this->whoami(); ?>
          
    <h3>Mejora tu cuenta a Premium!</h3>
    
    <div class="col-md-12 col-xs-12 col-sm-12">
        Disfruta de estas ventajas exclusivas de usuarios premium <strong>por solo 2,42 EUR / A&nacute;o</strong>
        <div style="clear: both;"></div>
        <div style="height: 15px;"></div>
        <div style="clear: both;"></div>
        <ul>
            <li>Crea todas las categorías que necesites</li>
            <li>Crea y comparte todas las cuentas que necesites</li>
            <li>Activa las notificaciones automáticas</li>
            <li>Exporta tus cuentas a excel</li>
            <li>Prueba las ultimas novedades</ul>
        </ul>
    </div>

    <div style="clear: both;"></div>
    <div style="height: 15px;"></div>
    <div style="clear: both;"></div>
    <div class="col-md-12 col-xs-12 col-sm-12 texto-espera hidden text-center">
        Espera unos segundos mientras te conectas con la pasarela de pago
        
        <div style="clear: both;"></div>
        <div style="height: 45px;"></div>
        <div style="clear: both;"></div>
        
    </div>
    
    

    <div class="col-md-12 col-xs-12 col-sm-12 text-center btn-share-bootstrap">
        <button class="btn btn-success btn-block" type="submit">Comparte en Facebook y gánate 1 mes gratis</button>
    </div>    
    
    <div class="col-md-12 col-xs-12 col-sm-12 text-center btn-share-fb hidden">
        <div class="fb-share-button" data-href="http://wallefit.com" data-layout="button" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwallefit.com%2F&amp;src=sdkpreparse">Compartir</a></div>
    </div>

  
    <div class="col-md-12 col-xs-12 col-sm-12 text-center" style="padding-top: 15px;">
        <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
        <input type="hidden" name="cmd" value="_s-xclick">
        <input type="hidden" name="hosted_button_id" value="6GSLPFW2XNU2G">
        <input class="launch_paypal" type="image" src="https://www.paypalobjects.com/es_ES/ES/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal, la forma rápida y segura de pagar en Internet.">
        <img alt="" border="0" src="https://www.paypalobjects.com/es_ES/i/scr/pixel.gif" width="1" height="1">
        </form>

    </div>
    

<?php
    require("layouts/footer.php");
?>