<?php
if (!defined('simplemvc_exec')) {
    die('No tiene acceso directo a este recurso');
}

extract($view_data);

$this->Header($view_data);
$this->addScripts('recovery_pass.js');
    
?>

    <div class="contenedor-login" style="text-align: center; margin-top:50px;">
    
   
        
        <form class="form-signin" method="post" action="">
            <h3>Wallefit</h3>
            <h4>Descubre agujeros, controla tu dinero</h4>
            
            <div style="clear: both;"></div>
            <div style="height: 40px;"></div>
            <div style="clear: both;"></div>
            
            <h4>Paso 1/3</h4>            
            <p>¿No recuerdas tu contraseña? Indica el nombre de usuario que usaste para el registro</p>
            
            <label for="name" name="name" class="sr-only">Nombre usuario</label>
            <input type="text" id="username" name="username" class="form-control" placeholder="usuario" maxlength="15" required>
            
            <?php if (isset($this->danger_message) && $this->danger_message != '') { ?>
                <div class="alert alert-danger">
                   <?php echo $this->danger_message; ?>
                </div>
                
                <div style="clear: both;"></div>
                <div style="height: 10px;"></div>
                <div style="clear: both;"></div>
            
            <?php } ?>
            
                <div style="clear: both;"></div>
                <div style="height: 10px;"></div>
                <div style="clear: both;"></div>                         
            
            <button class="btn btn-success btn-block" type="submit">Siguiente</button>
            <button class="btn btn-default btn-block btn-cancel" type="button">Cancelar</button>
            <input type="hidden" name="step" value="1">
            
        </form>
        
                <div style="clear: both;"></div>
                <div style="height: 20px;"></div>
                <div style="clear: both;"></div>         
        
        <span id="siteseal"><script async type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=wT0yKXwwTbQ5fuHy7974pH6Q0SQu3K6Ob82PbMgqPaTLkMXYYDnqLXVmWsfU"></script></span>
        
        
    </div>
      
<?php
    require("layouts/footer.php");
?>