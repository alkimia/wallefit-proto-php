<?php
if (!defined('simplemvc_exec')) {
    die('No tiene acceso directo a este recurso');
}

extract($view_data);

$this->Header($view_data);
$this->addScripts('recovery_pass.js');
    
?>
<div class="container-fluid">
    <h3>Wallefit</h3>
    <h4>Descubre agujeros, controla tu dinero</h4>
    
    <div style="clear: both;"></div>
    <div style="height: 40px;"></div>
    <div style="clear: both;"></div>

    <p><b>Clave reestablecida</b></h5>
    <p>Ahora puedes conectar con la nueva clave.</p>
    
    <button class="btn btn-success btn-cancel" type="button">Conectar</button>

</div> <!-- /container -->
<?php
    require("layouts/footer.php");
?>