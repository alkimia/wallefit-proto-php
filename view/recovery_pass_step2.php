<?php
if (!defined('simplemvc_exec')) {
    die('No tiene acceso directo a este recurso');
}

extract($view_data);

$this->Header($view_data);
$this->addScripts('recovery_pass.js');
    
?>

    <div class="contenedor-login" style="text-align: center; margin-top:50px;">
    
   
        
        <form class="form-signin" method="post" action="">
            <h3>Wallefit</h3>
            <h4>Descubre agujeros, controla tu dinero</h4>
            
            <div style="clear: both;"></div>
            <div style="height: 40px;"></div>
            <div style="clear: both;"></div>
            
             <h4>Paso 2/3</h4> 
                        
            <label>Pregunta secreta</label>            
            <p><?php echo $secret_question; ?></p>
            
            <label>Indica la respuesta secreta:</label>            
            <input type="text" id="secret_answer" name="secret_answer" class="form-control">
            
            <?php if (isset($this->danger_message) && $this->danger_message != '') { ?>
                <div class="alert alert-danger">
                   <?php echo $this->danger_message; ?>
                </div>
                
                <div style="clear: both;"></div>
                <div style="height: 10px;"></div>
                <div style="clear: both;"></div>
            
            <?php } ?>
            
                <div style="clear: both;"></div>
                <div style="height: 10px;"></div>
                <div style="clear: both;"></div>                         
            
            <button class="btn btn-success btn-block" type="submit">Siguiente</button>
            <button class="btn btn-default btn-block btn-cancel" type="button">Cancelar</button>
            
            <input type="hidden" name="step" value="2">
            <input type="hidden" name="username" value="<?php echo $this->urldata['username']; ?>">
            
        </form>
        
                <div style="clear: both;"></div>
                <div style="height: 20px;"></div>
                <div style="clear: both;"></div>         
        
        <span id="siteseal"><script async type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=wT0yKXwwTbQ5fuHy7974pH6Q0SQu3K6Ob82PbMgqPaTLkMXYYDnqLXVmWsfU"></script></span>
        
        
    </div>
      
<?php
    require("layouts/footer.php");
?>