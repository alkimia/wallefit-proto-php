<?php
if (!defined('simplemvc_exec')) {
    die('No tiene acceso directo a este recurso');
}

extract($view_data);

$this->Header($view_data);
$this->addScripts('register.js');
    
?>
<div class="container-fluid">

  <form class="form-signin" name="form_register" method="post" action="">
    <h3>Formulario de registro</h3>
    
<?php if ($register['message_error'] != '') { ?> 
    <div class="alert alert-danger">
      <strong>Ooops!</strong> <?php echo $register['message_error'] ; ?>
    </div>
<?php } ?>        
    <p>Usar&aacute;s estos datos para volver a entrar en Wallefit</p>
    <label for="name" name="name" class="sr-only">Nombre</label>
    <input type="text" id="name" name="name" maxlength="15" class="form-control username-register" placeholder="usuario" required>
    <label for="pass" name="password" class="sr-only">Password</label>
    <input type="password" id="pass" name="password" class="form-control" placeholder="password" required>
    
    <div style="clear: both;"></div>
    <div style="height: 20px;"></div>
    <div style="clear: both;"></div>
        
    <button class="btn btn-primary do-register" type="button">Registrarme</button>
    <button class="btn btn-default go-login" type="button">Ya dispongo de cuenta, entrar</button>    
    <input type="hidden" name="action" value="register">
  </form>
  
<script>
    Android.registerDevice('');
</script>   

</div> <!-- /container -->
<?php
    require("layouts/footer.php");
?>