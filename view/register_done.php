<?php
if (!defined('simplemvc_exec')) {
    die('No tiene acceso directo a este recurso');
}

extract($view_data);

$this->Header($view_data);
    
?>
<div class="container-fluid">

    <h3>Registro completado con exito</h3>
    <p>El registro se ha completado correctamente. Puede iniciar sesión <a href="index.php">aquí</a> con los datos facilitados.</p>

</div> <!-- /container -->
<?php
    require("layouts/footer.php");
?>