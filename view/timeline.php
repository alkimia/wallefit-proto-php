<?php
if (!defined('simplemvc_exec')) {
    die('No tiene acceso directo a este recurso');
}

extract($view_data);

$this->Header($view_data);
$this->addScripts('timeline.js');
    
?>
    <?php echo $this->whoami(); ?>
    
        <?php echo $this->messagesBox(); ?>
        
        <?php
            $params = array (
                "add_buttons" => false
            );  
            echo $this->accountFilter($params); 
        ?>            
        
        <button data-toggle="collapse" class="btn btn-primary btn-block" data-target="#filtros">Filtros</button>
        
        <div style="clear: both;"></div>
        <div style="height: 10px;"></div>
        <div style="clear: both;"></div>
        
        <div id="filtros" class="collapse">
            <form method="get" action="index.php" name="filter_detail">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Tipo movimiento</label>
                            <select name="type" id="type" class="form-control">
                                <option value="">Todos</option>
                                <option value="I" <?php if ($this->urldata['type']=='I') { echo ' selected'; } ?>>Ingreso</option>
                                <option value="O" <?php if ($this->urldata['type']=='O') { echo ' selected'; } ?>>Gasto</option>
                            </select>
                    </div>                        
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Texto</label>
                            <input type="text" name="keyword" class="form-control">
                    </div>                        
                </div>                

                <div class="form-group">
                <?php
                    if ($this->urldata['type'] == 'I') {
                        $hidden_outcome=' hidden';    
                    } elseif ($this->urldata['type'] == 'O') {
                        $hidden_outcome='';
                    } else {
                        $hidden_outcome=' hidden';
                    }
                                    
                ?>                
                    <div class="O_group <?php echo $hidden_outcome; ?>">
                      <label for="usr">Categoria gastos:</label>
                        <select name="group_outcome" class="form-control selector-outcome">
                        <option value="">- Selecciona</option>
                            <?php foreach($outcome_groups as $group) { ?>
                              <option value="<?php echo $group['id']; ?>" <?php if ($this->urldata['group_outcome'] == $group['id']) { echo ' selected'; } ?>><?php echo $group['descripcion']; ?></option>
                            <?php } ?>
                                <option value="-1">Agregar categoria</option>        
                        </select>
                  
                    </div>
                <?php
                    if ($this->urldata['type'] == 'I') {
                        $hidden_income='';    
                    } elseif ($this->urldata['type'] == 'O') {
                        $hidden_income=' hidden';
                    } else {
                        $hidden_income=' hidden';
                    }
                                    
                ?>                
                    
                    <div class="I_group <?php echo $hidden_income; ?>">
                      <label for="usr">Categoria ingresos:</label>
                        <select name="group_income" class="form-control selector-income">
                            <option value="">- Selecciona</option>
                            <?php foreach($income_groups as $group) { ?>
                              <option value="<?php echo $group['id']; ?>" <?php if ($this->urldata['group_income'] == $group['id']) { echo ' selected'; } ?>><?php echo $group['descripcion']; ?></option>
                            <?php } ?>
                                <option value="-1">Agregar categoria</option>            
                        </select>
                   
                    </div>            
               </div> 

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <label>Desde mes / a&nacute;o</label>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-8">
                        <select name="mm_from" class="form-control">
                        <?php
                        $parts_from_date = explode("-", $from_date);                    
                        
                        $month = $parts_from_date[1];
                        $year = $parts_from_date[0];
                        
                        if ($month<10) {
                            $month = str_replace("0", "", $month);
                        }                        
                        
                        $meses = array (
                            "01" => "Enero",
                            "02" => "Febrero",
                            "03" => "Marzo",
                            "04" => "Abril",
                            "05" => "Mayo",
                            "06" => "Junio",
                            "07" => "Julio",
                            "08" => "Agosto",
                            "09" => "Septiembre",
                            "10" => "Octubre",
                            "11" => "Noviembre",
                            "12" => "Diciembre"
                        );
                        
                        for($i = 1; $i < 13; ++$i) {
                            if ($i < 10) {
                                $idx = '0' . $i;
                            }
                            
                            ?>
                            <option value="<?php echo $idx; ?>" <?php if ($month == $idx) { echo ' selected'; } ?>><?php echo $meses[$idx]; ?></option>
    <?php } ?>                    
                        </select>
                    </div>    
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <select name="yy_from" class="form-control">
    <?php
                        $year_init = date("Y") - 2;
                        $year_end = date("Y") + 1;
                        for($i = $year_init ; $i < $year_end; ++$i) { ?>
                            <option value="<?php echo $i; ?>" <?php if ($year == $i) { echo ' selected'; } ?>><?php echo $i; ?></option>
                        <?php } ?>                    
                        </select>
                    </div>    
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Hasta mes / a&nacute;o</label>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-8">
                        <select name="mm_to" class="form-control">
                        <?php
                        $parts_to_date = explode("-", $to_date);                    
                        
                        $month = $parts_to_date[1];
                        $year = $parts_to_date[0];
                        
                        if ($month<10) {
                            $month = str_replace("0", "", $month);
                        }                         
                        

                        for($i = 1; $i < 13; ++$i) { 

                            if ($i < 10) {
                                $idx = '0' . $i;
                            }                            
                            
                            ?>
                            <option value="<?php echo $idx; ?>" <?php if ($month == $idx) { echo ' selected'; } ?>><?php echo $meses[$idx]; ?></option>
    <?php } ?>                    
                        </select>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <select name="yy_to" class="form-control">
    <?php
                        $year_init = date("Y") - 2;
                        $year_end = date("Y") + 1;
                        for($i = $year_init ; $i < $year_end; ++$i) { ?>
                            <option value="<?php echo $i; ?>" <?php if ($year == $i) { echo ' selected'; } ?>><?php echo $i; ?></option>
                        <?php } ?>                    
                        </select>
                    </div>                                            
                </div>                                 
            </div>
            <input type="hidden" name="action" id="action" value="filters">
            <input type="hidden" name="mov_id" id="mov_id">
            <input type="hidden" name="id_account" id="id_account_sel" value="<?php echo $id_account; ?>">
            <input type="hidden" name="page" value="timeline">

            
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                    <button type="submit" class="btn btn-danger btn-block text-center">Aplicar filtro</button>
                </div>
            </div>
            </form>

            <div style="clear: both;"></div>
            <div style="height: 10px;"></div>
            <div style="clear: both;"></div>
            
            <h4>Detalle</h4>
            
            <div style="clear: both;"></div>
            <div style="height: 10px;"></div>
            <div style="clear: both;"></div>
        </div>        

        <?php
        if ($timeline) { 
            foreach ($timeline as $item_time) {
                
                echo $this->card_timelime_detail($item_time);
                    
            }
         } else { ?>
            <div class="alert alert-info">
              <strong>No hay datos!</strong> Prueba modificando los datos del filtro
            </div>            
        <?php } ?>
       </div>

<?php
    require("layouts/footer.php");
?>